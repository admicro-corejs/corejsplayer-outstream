# [Video.js - CoreJS Outstream Player][vjs]

> Currently, advertising by video is a important part, IAB release Digital Video Ad Serving Template (VAST) standard, Video Player Ad-Serving Interface Definition (VPAID).
> This player serve for it.

## Table of Contents

* [Quick Start](#quick-start)

## Quick Start

Add these tags to your document's `<head>`:

```html
<link href="https://media1.admicro.vn/cms/corejs-player-outstream-css.min.css" rel="stylesheet" />
<script src="https://media1.admicro.vn/cms/corejsplayer-outstream.min.js"></script>
```

```html
<div id="playerId"></div>
<script>
  window.playerInstance = new CoreJSPlayer(playerId); (0)
      playerInstance.initialize({
        width, (1)
        height, (2)
        poster,  (3)
        loop: false, (5)
        muted: true, (6)
        template: {  (7)
          name: 'catfish', (8)
          timeout: 15,  (9)
          height: 'auto',  (10)
          timeToShowSkip: 5  (11)
        },
        logo: {  (12)
          img: (13) 'https://adi.admicro.vn/adt/cpc/ssvimg/mobile/bg/logo-admicro.png',
          redirectUrl: 'https://admicro.vn/',  (14)
          position: 'top-right'  (15)
        },
        ads: { (16)
          adTagUrl, (17)
          vastXml, (18)
          disableAdControls: false, (19)
          disableSkipAdButton: false, (20)
          showCountdown: true, (21)
          viewable: { (22)
            percent: percent, (23)
            timeIn: timeIn (24)
          },
          backupImg (25),
          repeatClick: true, (26)
        }
      })
</script>
```
