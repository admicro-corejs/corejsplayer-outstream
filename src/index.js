import './vendors/polyfills/objectAssign';
import './vendors/polyfills/replacewith';
import document from 'global/document';
import window from 'global/window';
import sdk from './sdk';
import EventHandler from './events/main';
import videojs from './player/js';
import PlayerConfig from './models/PlayerConfig';
import Catfish from './templates/catfish';
import * as load from './utils/load';
import debuger from './sdk/AdDebug';
import ControllerDefault from './plugins/Controller';
import Logging from './sdk/AdsLogging';
// import * as browser from './player/js/utils/browser.js';

const globalEvents = window.__corejsPlayerConfig || [];

// const element = document.createElement('video');
// as of videojs 6.6.0
const DEFAULT_EVENTS = [
  'loadeddata',
  'canplay',
  'canplaythrough',
  'play',
  'pause',
  'waiting',
  'playing',
  'ended',
  'error',
  'fullscreenchange'
];

videojs.options.autoSetup = false;
class CoreJSPlayer {
  constructor(id) {
    this.config = new PlayerConfig({});
    this.id = id;
    this.element = null;
    this.player = null;
    this.eventHandler = new EventHandler();
    this.adsOptions = null;
    this.element = null;
    this.source = '';
    this.CurrentBaseController = ControllerDefault;

    this.adLoader = null;
    /**
     * SDK AdsLoader
     */
    this.adsLoader = null;

    /**
     * SDK AdsManager
     */
    this.adsManager = null;
    this.controller = null;
  }

  loadCss(onload) {
    const cssUrl = '//media1.admicro.vn/cms/corejs-player-css.min.css';
    const isInstall = document.getElementById('corejs-player-css');

    if (!isInstall) {
      load.loadCss(cssUrl, () => {
        onload();
      }, 'corejs-player-css');
    }
  }

  emitEvent(event, value) {
    if (event) {
      this.eventHandler.handler(event, this.adsManager, this.player, value);
    }
    // debuger.pushEvent(event, null, value);
  }

  idGenerate() {
    return Math.floor(Math.random() * 100000000);
  }

  createElement() {
    this.element = document.createElement('video');
    this.element.className = 'video-corejs';
    this.element.id = this.id;
    const playsinline = document.createAttribute('playsinline');
    const webkitPlaysinline = document.createAttribute('webkit-playsinline');

    this.element.setAttributeNode(playsinline);
    this.element.setAttributeNode(webkitPlaysinline);
  }

  replaceTarget() {
    const target = document.getElementById(this.id);

    target.replaceWith(this.element);
  }

  initAdObject(player) {
    this.adsLoader = new sdk.AdsLoader(player);

    this.adsLoader.addEventListener(
      sdk.Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
      this.onAdsManagerLoaded.bind(this),
      false
    );

    this.adsLoader.addEventListener(
      sdk.Event.AdsErrorEvent.Type.AD_ERROR,
      this.onAdsLoaderError.bind(this),
      false
    );
  }

  onAdsManagerLoaded(adsManagerLoadedEvent) {
    this.adsManager = adsManagerLoadedEvent.getAdsManager();
    this.emitEvent('ad-manager-loaded', this.adsManager);
    this.adsManager.on(sdk.Event.AdsEvent.Type.TRUE_VIEW, (time) => {
      this.emitEvent(sdk.Event.AdsEvent.Type.TRUE_VIEW, time);
    });
    this.adsManager.on(sdk.Event.AdsEvent.Type.CLICK, (time) => {
      this.emitEvent(sdk.Event.AdsEvent.Type.CLICK, time);
    });
    this.controller = new this.CurrentBaseController(this.player, this.adsManager);
    this.controller.onAdsManagerLoaded(adsManagerLoadedEvent);
    this.adsManager.adUi = this.controller.adUi;

    this.adsManager.autoPlay();

    this.adsManager.on('tick', (value) => {
      this.emitEvent('tick', value);
    });

    this.adsManager.on(sdk.Event.AdsEvent.Type.LOADED, (ads) => {
      this.emitEvent(sdk.Event.AdsEvent.Type.LOADED, ads);
    });

    this.adsManager.on('autoplayerror', () => {
      this.emitEvent('autoplayerror');
    });

    this.adsManager.on('vasttrackinginitialized', () => {
      this.adsManager.vastTracker.setCorejsTracking((eventDetail) => {
        this.emitEvent(eventDetail.event, eventDetail.value);
      });
    });

    this.emitEvent(sdk.Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED);

    this.eventHandler.register(sdk.Event.AdsEvent.Type.STARTED, (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAdStart(this.adsManager.currentAds.id, impression ? impression.url : null);
    });

    this.eventHandler.register(sdk.Event.AdsEvent.Type.PAUSED, (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAdPause(this.adsManager.currentAds.id, impression ? impression.url : null);
    });

    this.eventHandler.register('timeupdate', (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAdPoll(this.adsManager.currentAds.id, impression ? impression.url : null);
    }, 'heartbeat');

    this.eventHandler.register('timeupdate', (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAd25(this.adsManager.currentAds.id, impression ? impression.url : null);
    }, 'percent', 0.25);

    this.eventHandler.register('timeupdate', (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAd50(this.adsManager.currentAds.id, impression ? impression.url : null);
    }, 'percent', 0.5);

    this.eventHandler.register('timeupdate', (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAd75(this.adsManager.currentAds.id, impression ? impression.url : null);
    }, 'percent', 0.75);

    this.eventHandler.register('timeupdate', (event, value, type) => {
      const impression = this.adsManager.currentAds.impressionURLTemplates.filter(i => i.id === 'Admicro')[0];

      this.logging.logAd100(this.adsManager.currentAds.id, impression ? impression.url : null);
    }, 'percent', 1);
  }

  onAdsLoaderError() {
    this.emitEvent('adError');
    console.log('AdsLoaderError'); // eslint-disable-line
    if (this.adsManager) {
      this.adsManager.destroy();
    }
  }

  createAdRerequest(adOptions) {
    const adsRequest = new sdk.AdsRequest();

    if (adOptions.adTagUrl) {
      adsRequest.adTagUrl = adOptions.adTagUrl;
    } else {
      adsRequest.adsResponse = adOptions.adsResponse;
    }

    if (adOptions.vastXml) {
      adsRequest.vastXml = adOptions.vastXml;
    }

    if (adOptions.viewable) {
      adsRequest.viewable = adOptions.viewable;
    }
    if (adOptions.forceNonLinearFullSlot) {
      adsRequest.forceNonLinearFullSlot = true;
    }

    if (adOptions.vastLoadTimeout) {
      adsRequest.vastLoadTimeout = adOptions.vastLoadTimeout;
    }

    adsRequest.linearAdSlotWidth = 0;
    adsRequest.linearAdSlotHeight = 0;
    // adsRequest.nonLinearAdSlotWidth = adOptions.nonLinearWidth || this.controller.getPlayerWidth();
    // adsRequest.nonLinearAdSlotHeight = adOptions.nonLinearHeight || this.controller.getPlayerHeight();
    adsRequest.setAdWillAutoPlay(true);
    adsRequest.setAdWillPlayMuted(true);
    return adsRequest;
  }

  requestAds(adOptions) {
    const adsRequest = this.createAdRerequest(adOptions);

    this.adsLoader.requestAds(adsRequest, adOptions);
  }

  /**
   *
   * @param {object} config
      * @property width {number}
      * @property height {number}
      * @property sources {array}
      * @property poster {string}
      * @property autoPlay {boolean}
      * @property controls {boolean}
      * @property ads {object}:
      * @property adTagUrl {string}
      * @property viewable {object}: percent,timeIn
      * @property repeatClick {boolean}
   */
  initialize(options) {
    // if (browser.IS_CHROME && browser.IS_IOS) {
    //   return;
    // }
    this.createElement();
    // videojs options
    const videoOptions = Object.assign({}, this.config.globalOptions, this.config.options, options);

    videoOptions.ads.outstream = videoOptions.outstream;

    if (videoOptions.template) {
      switch (videoOptions.template.name) {
      case 'catfish':
        const template = new Catfish(this.id, videoOptions.template);

        if (template.CatfishController) {
          this.CurrentBaseController = template.CatfishController;
        }
        template.init();

        const demension = template.getDemension();

        if (!videoOptions.width) {
          videoOptions.width = demension.width;
        }

        if (!videoOptions.height) {
          videoOptions.height = demension.height;
        }

        this.eventHandler.register(sdk.Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, (event, player, type, value) => {
          template.setAdManager(this.adsManager);
          template.setEventHandler(this.eventHandler);
        }, null);
        break;

      default:
        break;
      }
    }

    this.replaceTarget();

    // avoid error "VIDEOJS: ERROR: Unable to find plugin: __ob__"
    if (videoOptions.file) {
      videoOptions.sources = [videoOptions.file];
    }
    if (videoOptions.plugins) {
      delete videoOptions.plugins.__ob__;
    }
    // videoOptions
    console.log('videoOptions', videoOptions); // eslint-disable-line

    // player
    const self = this;

    this.player = videojs(this.element, videoOptions, function() {
      // events
      const events = DEFAULT_EVENTS.concat(self.events).concat(globalEvents);

      // watch events
      const onEdEvents = {};

      for (let i = 0; i < events.length; i += 1) {
        if (typeof events[i] === 'string' && onEdEvents[events[i]] === undefined) {
          (event => {
            onEdEvents[event] = null;
            this.on(event, () => {
              self.emitEvent(event, true);
            });
          })(events[i]);
        }
      }
      // watch timeupdate
      this.on('timeupdate', function() {
        self.emitEvent('timeupdate', this.currentTime());
      });
      // player readied
      self.emitEvent('ready', this);
    });

    this.logging = new Logging(this.player);
    this.initAdObject(this.player);
    this.requestAds(videoOptions.ads);
  }
}

window.CoreJSPlayer = CoreJSPlayer;
window.coreJsDebugger = debuger;

export {
  videojs,
  PlayerConfig,
  CoreJSPlayer
};

