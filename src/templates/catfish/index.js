import document from 'global/document';
import window from 'global/window';
import sdk from '../../sdk';

import CatfishController from './Controller';

class Catfish {
  constructor(id, templateOptions) {
    this.template = null;
    this.adsManager = null;
    this.id = id;
    this.CatfishController = CatfishController;
    this.name = templateOptions.name;
    this.timeToShowSkip = templateOptions.timeToShowSkip;
    this.timeout = templateOptions.timeout;
    this.options = templateOptions;
    this.isShowSkip = false;
  }

  getTimeToShowSkip() {
    return isNaN(this.timeToShowSkip) ? 5000 : this.timeToShowSkip;
  }

  getDemension() {
    const screenWidth = window.screen.width;
    const demension = {
      width: screenWidth
    };

    if (this.options.height === 'auto') {
      return demension;
    }

    if (screenWidth > 320) {
      demension.height = 160;
    }
    demension.height = 140;

    return demension;
  }

  setAdManager(adsManager) {
    this.adsManager = adsManager;

    // this.adsManager.on(sdk.Event.AdsEvent.Type.LOADED, () => {
    //   this.init();
    // });

    setTimeout(() => {
      if (!this.isShowSkip) {
        this.isShowSkip = true;
        this.adsManager.adUi.showSkipAd();
      }
    }, 10000);

    this.adsManager.on('force-destroy', () => {
      this.forceDestroy();
    });

    if (this.timeout) {
      console.log('listen ad skip'); // eslint-disable-line
      this.adsManager.on(sdk.Event.AdsEvent.Type.SKIPPED, () => {
        this.destroy();
      });
    } else {
      console.log('listen ad complete'); // eslint-disable-line
      this.adsManager.on(sdk.Event.AdsEvent.Type.COMPLETE, () => {
        this.destroy();
      });
      this.adsManager.on('autoplayerror', () => {
        this.destroy();
      });
    }
  }

  setEventHandler(eventHandler) {
    if (this.timeout) {
      eventHandler.register('timeupdate', () => {
        this.adsManager.skipAd();
      }, 'time', this.timeout);
    }

    if (!isNaN(this.timeToShowSkip)) {
      eventHandler.register('timeupdate', () => {
        if (!this.isShowSkip) {
          this.isShowSkip = true;
          this.adsManager.adUi.showSkipAd();
        }
      }, 'time', this.getTimeToShowSkip());

      eventHandler.register('tick', () => {
        if (!this.isShowSkip) {
          this.isShowSkip = true;
          this.adsManager.adUi.showSkipAd();
        }
      }, 'time', this.getTimeToShowSkip());
    }
  }

  init() {
    const target = document.getElementById(this.id);
    const template = document.createElement('div');

    template.style.width = `${this.getDemension().width}px`;

    template.setAttribute('class', 'corejs-animate-bottom');
    template.innerHTML = `<div id="${this.id}"></div>`;
    target.replaceWith(template);
    this.template = template;
  }

  forceDestroy() {
    this.adsManager.skipAd();
    this.destroy();
  }

  destroy() {
    this.adsManager.adsLoader.player.muted(true);
    this.template.setAttribute('class', 'corejs-animate-bottom-out');
    setTimeout(() => {
      this.template.remove();
    }, 3000);
  }
}

export default Catfish;
