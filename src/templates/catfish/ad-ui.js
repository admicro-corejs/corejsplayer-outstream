import document from 'global/document';
import window from 'global/window';
import AdUi from '../../plugins/ad-ui/ad-ui';

export default class CatFishAdUi extends AdUi {
  constructor(player, adManager) {
    super(player, adManager);
    this.name = 'catFishAdUi';
    this.skipDiv.innerHTML = '<div class="control skip" style=""><div><span></span></div></div>';
    this.muteDiv.innerHTML = '<div><span>&nbsp;</span></div>';
    this.playPauseDiv.innerHTML = '<div><span></span></div>';
    this.fullscreenDiv.innerHTML = '<div><span></span></div>';
    this.videoPauseOverlay = document.createElement('div');
    this.bigPlayBtnDiv = document.createElement('div');
    this.bigPlayBtnDiv.innerHTML = '<div><span></span></div>';
    this.catfishControlsDiv = document.createElement('div');

    this.createAdContainer();
  }

  get classes() {
    return {
      skipDiv: 'corejs-catfish-skip-control',
      clickCoverDiv: 'corejs-click-cover-div',
      playPauseDivStatusPlaying: 'control pause',
      playPauseDivStatusPause: 'control play',
      muteDivStatusUnmute: 'control volume volume-on',
      muteDivStatusMuted: 'control volume volume-off',
      videoPauseOverlay: 'corejs-catfish-video-paused-overlay',

      // muteDivForMobile: 'corejs-mute-mobile-div',
      // fullscreenDivForMobile: '',
      fullscreenStatusNonFullScreen: 'control full',
      fullscreenStatusFullScreen: 'control exit'
    };
  }

  createControls() {
    this.assignControlAttributes(this.controlsDiv, 'corejs-controls-div');
    this.controlsDiv.style.width = '100%';

    this.addClass(this.catfishControlsDiv, 'corejs-catfish-video-controls');
    this.catfishControlsDiv.style.width = '100%';

    this.assignControlAttributes(this.countdownDiv, 'corejs-countdown-div');
    this.countdownDiv.innerHTML = this.default.adLabel;
    this.countdownDiv.style.display = this.showCountdown ? 'block' : 'none';
    this.assignControlAttributes(this.skipDiv, 'corejs-skip-div');
    this.addClass(this.skipDiv, this.classes.skipDiv);
    // this.skipDiv.innerHTML = this.default.skipLabel;
    this.skipDiv.style.display = 'none';
    this.skipDiv.addEventListener(
      'click',
      this.onSkipAd.bind(this),
      false
    );

    const logoOptions = this.player.options().logo;

    this.logoDiv.style.background = `url('${logoOptions.img}') no-repeat`;
    this.addClass(this.logoDiv, `corejs-logo ${logoOptions.position}`);
    this.logoDiv.addEventListener('click', function() {
      window.open(logoOptions.redirectUrl);
    });

    this.addClass(this.videoPauseOverlay, this.classes.videoPauseOverlay);
    this.videoPauseOverlay.style.display = 'none';
    this.addClass(this.bigPlayBtnDiv, this.classes.playPauseDivStatusPause);
    this.videoPauseOverlay.appendChild(this.bigPlayBtnDiv);
    this.bigPlayBtnDiv.addEventListener(
      'click',
      this.onAdPlayPauseClick.bind(this),
      false
    );

    this.assignControlAttributes(this.clickCoverDiv, this.classes.clickCoverDiv);
    this.addClass(this.clickCoverDiv, this.classes.clickCoverDiv);

    this.assignControlAttributes(this.seekBarDiv, 'corejs-seek-bar-div');
    this.seekBarDiv.style.width = '100%';

    this.assignControlAttributes(this.progressDiv, 'corejs-progress-div');

    // this.assignControlAttributes(this.playPauseDiv, 'corejs-play-pause-div');

    this.addClass(this.playPauseDiv, this.classes.playPauseDivStatusPlaying);
    this.playPauseDiv.addEventListener(
      'click',
      this.onAdPlayPauseClick.bind(this),
      false
    );

    // this.assignControlAttributes(this.muteDiv, 'corejs-mute-div');
    // if (this.isMobile) {
    //   this.addClass(this.muteDiv, this.classes.muteDivForMobile);
    // }
    if (!this.player.muted()) {
      this.addClass(this.muteDiv, this.classes.muteDivStatusUnmute);
    } else {
      this.addClass(this.muteDiv, this.classes.muteDivStatusMuted);
    }
    this.muteDiv.addEventListener(
      'click',
      this.onAdMuteClick.bind(this),
      false
    );

    this.assignControlAttributes(this.sliderDiv, 'corejs-slider-div');
    this.sliderDiv.addEventListener(
      'mousedown',
      this.onAdVolumeSliderMouseDown.bind(this),
      false
    );

    // Hide volume slider controls on iOS as they aren't supported.
    if (this.isIos) {
      this.sliderDiv.style.display = 'none';
    }

    this.assignControlAttributes(this.sliderLevelDiv, 'corejs-slider-level-div');

    // this.assignControlAttributes(this.fullscreenDiv, 'corejs-fullscreen-div');
    this.addClass(this.fullscreenDiv, this.classes.fullscreenStatusNonFullScreen);
    this.fullscreenDiv.addEventListener(
      'click',
      this.onAdFullscreenClick.bind(this),
      false
    );

    this.adContainerDiv.appendChild(this.skipDiv);
    this.adContainerDiv.appendChild(this.controlsDiv);
    this.controlsDiv.appendChild(this.countdownDiv);
    this.controlsDiv.appendChild(this.seekBarDiv);
    if (this.isMobile) {
      this.catfishControlsDiv.appendChild(this.playPauseDiv);
      this.catfishControlsDiv.appendChild(this.muteDiv);
      this.catfishControlsDiv.appendChild(this.fullscreenDiv);
    } else {
      this.controlsDiv.appendChild(this.playPauseDiv);
      this.controlsDiv.appendChild(this.muteDiv);
      this.controlsDiv.appendChild(this.fullscreenDiv);
    }
    this.adContainerDiv.appendChild(this.catfishControlsDiv);
    this.controlsDiv.appendChild(this.sliderDiv);
    this.seekBarDiv.appendChild(this.progressDiv);
    this.sliderDiv.appendChild(this.sliderLevelDiv);
    this.adContainerDiv.appendChild(this.skipAd);
    this.adContainerDiv.appendChild(this.clickCoverDiv);
    this.adContainerDiv.appendChild(this.videoPauseOverlay);
    this.adContainerDiv.appendChild(this.logoDiv);
  }

  /**
 * Shows ad controls on mouseover.
 */
  showAdControls() {
    return;
  }

  onAdsPlaying() {
    super.onAdsPlaying();
    this.hideVideoPauseOverlay();
  }

  onAdsPaused() {
    super.onAdsPaused();
    this.showVideoPauseOverlay();
  }

  showVideoPauseOverlay() {
    this.videoPauseOverlay.style.display = 'block';
    this.player.el().style.backgroundColor = 'transparent';
    this.contentPlayer.style.opacity = 0;
  }

  hideVideoPauseOverlay() {
    this.videoPauseOverlay.style.display = 'none';
    this.player.el().style.backgroundColor = '#000';
    this.contentPlayer.style.opacity = 1;
  }

}
