import Controller from '../../plugins/Controller';
import AdUi from './ad-ui';

export default class CatfishController extends Controller {
  constructor(player, adsManager) {
    super(player, adsManager);
    this.adUi = new AdUi(player, adsManager);

    this.registerHandlerForAdUi();
    this.trackPlayPause();
    this.trackFullScreen();
    this.trackMute();
  }
}
