import window from 'global/window';
import document from 'global/document';

export default function(callback) {
  function pageAwakened() {
    // add code here to remove duplicate events. Check !document.hidden if supported
    callback();
  }

  window.addEventListener('focus', pageAwakened);
  window.addEventListener('pageshow', pageAwakened);
  window.addEventListener('visibilitychange', function() {
    !document.hidden && pageAwakened(); // eslint-disable-line
  });
}
