import document from 'global/document';
import isFunction from '../vendors/view-tracking/util/isFunction';
/**
 * @function {installScript} excute js from url
 * @param {*} el : place to install script.
 * @param {*} url : url of script.
 * @param {*} executedTime : time to execute.
 */
export function loadScript(el, url, isAsync, executedTime) {
  const asyncFlag = (typeof isAsync === 'boolean') ? isAsync : true;

  return new Promise((resolve) => {
    const newScriptTag = document.createElement('script');

    newScriptTag.type = 'text/javascript';
    newScriptTag.onload = function() {
      setTimeout(() => {
        resolve('installScriptDone');
      }, ((executedTime) || 0));
    };
    newScriptTag.onerror = function(e) {
      setTimeout(() => {
        resolve('installScriptDone');
        throw e;
      }, ((executedTime) || 0));
    };
    newScriptTag.async = asyncFlag;
    newScriptTag.src = url;
    if (!el) {
      const head = document.getElementsByTagName('head')[0];

      head.appendChild(newScriptTag);
    } else {
      el.appendChild(newScriptTag);
    }
  });
}

export function loadCss(url, onload, id) {
  const head = document.getElementsByTagName('head')[0];
  const link = document.createElement('link');

  if (id) {
    link.id = id;
  }
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = url;
  link.media = 'all';
  if (isFunction(onload)) {
    link.onload = onload;
  }
  head.appendChild(link);
}
