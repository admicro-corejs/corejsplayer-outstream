
import window from 'global/window';

export default class LocalStorage {
  constructor() {
    this.timestamp = 'timestamp_';
    this.storage = (function() {
      try {
        return window.localStorage || window.sessionStorage;
      } catch (error) {
        return null;
      }
    }());
  }
  getExpires(cookie, type, start, endKey) {
    if (!this.storage) {
      return null;
    }
    const cookieTmp = `${cookie}`;
    const cookieStart = cookieTmp.indexOf(type, start);
    const cookLength = cookieTmp.length - 1;

    if (cookieStart !== -1) {
      let cookieEnd = cookieTmp.indexOf(endKey, cookieStart);

      if (cookieEnd === -1) {
        cookieEnd = cookLength;
      }
      return cookieTmp.substring(cookieStart, cookieEnd);
    }
    return '';
  }
  setItem(key, value, expires) {
    if (!this.storage) {
      return;
    }
    let endChar = '';
    const date = new Date();
    let time = date.getTime();
    const timestamp = this.timestamp;

    if (expires === 0 || expires === '') {
      time += 864 * 1e5;
    } else {
      // time count in minute
      time += expires * 6 * 1e4;
    }
    if (key === '_azs') {
      endChar = ';';
    } else {
      endChar = ',';
    }
    const strTimeStamp = this.getExpires(value, timestamp, 0, endChar);
    let valueTmp = value;

    if (strTimeStamp === '') {
      valueTmp += timestamp + time.toString() + endChar;
    } else {
      valueTmp = value.replace(strTimeStamp, timestamp + time.toString());
    }
    this.storage.setItem(key, valueTmp);
  }
  getItem(key, chkTime) {
    if (!this.storage) {
      return null;
    }
    let a = this.storage.getItem(key);
    let endChar = '';
    const date = new Date();
    const time = date.getTime();
    const timestamp = this.timestamp;

    if (a === '' || a === null) {
      return '';
    } else if (key === '_azs') {
      endChar = ';';
    } else {
      endChar = ',';
    }
    let strTimeStamp = this.getExpires(a, timestamp, 0, endChar);

    strTimeStamp = strTimeStamp.replace(timestamp, '');
    if (strTimeStamp === '' || isNaN(parseInt(strTimeStamp, 10)) || parseInt(strTimeStamp, 10) < time) {
      return '';
    }

    if (typeof (chkTime) !== 'undefined') {
      a = a.replace(timestamp + strTimeStamp + endChar, '');
    }
    return a;
  }
  removeItem(key) {
    if (!this.storage) {
      return;
    }
    this.storage.removeItem(key);
  }
  // clear all key-value item
  flush() {
    if (!this.storage) {
      return;
    }
    this.storage.clear();
  }
}
