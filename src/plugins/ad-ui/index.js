import AdUi from './ad-ui';

export default class AdUiDefault extends AdUi {
  constructor(player, adManager) {
    super(player, adManager);
    this.name = 'default';
    this.createAdContainer();
  }
}
