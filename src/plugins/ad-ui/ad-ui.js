/* eslint-disable no-inline-comments,eqeqeq */
import document from 'global/document';
import window from 'global/window';

/**
 * Ad UI implementation.
 *
 * @param {Controller} controller Plugin controller.
 * @class
 * @struct
 * @final
 */

class AdUi {
  constructor(player, adManager) {
    this.player = player;
    this.adManager = adManager;

    this.contentPlayer = this.player.el().getElementsByClassName('vjs-tech')[0];

    this.isIpados = window.navigator.userAgent.match(/Macintosh/i) && 'ontouchend' in document;

    /**
     * Whether or not we are running on a mobile platform.
     */
    this.isMobile = (window.navigator.userAgent.match(/iPhone/i) ||
    window.navigator.userAgent.match(/iPad/i) ||
    window.navigator.userAgent.match(/Android/i)) ||
    this.isIpados;

    /**
     * Whether or not we are running on an iOS platform.
     */
    this.isIos = (window.navigator.userAgent.match(/iPhone/i) ||
    window.navigator.userAgent.match(/iPad/i));

    this.clickCoverDiv = document.createElement('div');

    this.skipAd = document.createElement('div');

    this.logoDiv = document.createElement('div');

    /**
     * backup when cant auto play
     */
    this.imgBackup = document.createElement('img');

    /**
     * Div used as an ad container.
     */
    this.adContainerDiv = document.createElement('div');
    this.vpaidContainerDiv = document.createElement('div');

    /**
     * Div used to skip ad
     */
    this.skipDiv = document.createElement('div');

    /**
     * Div used to display ad controls.
     */
    this.controlsDiv = document.createElement('div');

    /**
     * Div used to display ad countdown timer.
     */
    this.countdownDiv = document.createElement('div');

    /**
     * Div used to display add seek bar.
     */
    this.seekBarDiv = document.createElement('div');

    /**
     * Div used to display ad progress (in seek bar).
     */
    this.progressDiv = document.createElement('div');

    /**
     * Div used to display ad play/pause button.
     */
    this.playPauseDiv = document.createElement('div');

    /**
     * Div used to display ad mute button.
     */
    this.muteDiv = document.createElement('div');

    /**
     * Div used by the volume slider.
     */
    this.sliderDiv = document.createElement('div');

    /**
     * Volume slider level visuals
     */
    this.sliderLevelDiv = document.createElement('div');

    /**
     * Div used to display ad fullscreen button.
     */
    this.fullscreenDiv = document.createElement('div');

    /**
     * Bound event handler for onMouseUp.
     */
    this.boundOnMouseUp = this.onMouseUp.bind(this);

    /**
     * Bound event handler for onMouseMove.
     */
    this.boundOnMouseMove = this.onMouseMove.bind(this);

    /**
     * Stores data for the ad playhead tracker.
     */
    this.adPlayheadTracker = {
      currentTime: 0,
      duration: 0,
      isPod: false,
      adPosition: 0,
      totalAds: 0
    };

    /**
     * Used to prefix videojs ima controls.
     */
    this.controlPrefix = this.player.id() + '_';

    /**
     * Boolean flag to show or hide the ad countdown timer.
     */
    this.showCountdown = true;
    if (this.player.options().ads.showCountdown === false) {
      this.showCountdown = false;
    }

    this.adPlayPauseClickHandler = null;
    this.adMuteClickHandler = null;
    this.adFullScreenClickHandler = null;

    this.muteAction = null;
    this.changeVolumeAction = null;
    // this.createAdContainer();
  }

  get classes() {
    return {
      skipDiv: 'corejs-skip-catfish-button',
      clickCoverDiv: 'corejs-click-cover-div',
      playPauseDivStatusPlaying: 'corejs-playing',
      playPauseDivStatusPause: 'corejs-paused',
      playPauseDivForMobile: 'corejs-play-pause-mobile-div',
      muteDivStatusUnmute: 'corejs-non-muted',
      muteDivStatusMuted: 'corejs-muted',
      muteDivForMobile: 'corejs-mute-mobile-div',
      fullscreenDivForMobile: '',
      fullscreenStatusNonFullScreen: 'corejs-non-fullscreen',
      fullscreenStatusFullScreen: 'corejs-fullscreen'
    };
  }

  get default() {
    return {
      adLabel: 'Quảng cáo sẽ đóng sau',
      skipLabel: 'Skip Ad',
      adLabelNofN: 'of',
      debug: false,
      disableSkipAdButton: false,
      disableAdControls: false,
      prerollTimeout: 1000,
      preventLateAdStart: false,
      requestMode: 'onLoad',
      showControlsForJSAds: true,
      showCountdown: true,
      timeout: 5000
    };
  }

  /**
 * Creates the ad container.
 */
  createAdContainer() {
    this.assignControlAttributes(this.adContainerDiv, 'corejs-ad-container');
    this.adContainerDiv.style.position = 'absolute';
    this.adContainerDiv.style.zIndex = 1111;
    this.adContainerDiv.addEventListener(
      'mouseenter',
      this.showAdControls.bind(this),
      false
    );
    this.adContainerDiv.addEventListener(
      'mouseleave',
      this.hideAdControls.bind(this),
      false
    );
    this.assignControlAttributes(this.vpaidContainerDiv, 'corejs-vpaid-container');
    this.createControls();
    this.player.getChild('controlBar').el().parentNode.appendChild(this.adContainerDiv);
    this.adContainerDiv.appendChild(this.vpaidContainerDiv);
    // this.player.getChild('controlBar').el().parentNode.appendChild(this.vpaidContainerDiv);
  }

  /**
 * Create the controls.
 */
  createControls() {
    this.assignControlAttributes(this.controlsDiv, 'corejs-controls-div');
    this.controlsDiv.style.width = '100%';

    this.assignControlAttributes(this.countdownDiv, 'corejs-countdown-div');
    this.countdownDiv.innerHTML = this.default.adLabel;
    this.countdownDiv.style.display = this.showCountdown ? 'block' : 'none';
    // if (!this.isMobile) {
    //   this.assignControlAttributes(this.countdownDiv, 'corejs-countdown-div');
    //   this.countdownDiv.innerHTML = this.default.adLabel;
    //   this.countdownDiv.style.display = this.showCountdown ? 'block' : 'none';
    // } else {
    //   this.countdownDiv.style.display = 'none';
    // }
    const logoOptions = this.player.options().logo;

    this.logoDiv.style.background = `url('${logoOptions.img}') no-repeat`;
    this.addClass(this.logoDiv, `corejs-logo ${logoOptions.position}`);
    this.logoDiv.addEventListener('click', function() {
      window.open(logoOptions.redirectUrl);
    });

    this.assignControlAttributes(this.skipDiv, 'corejs-skip-div');
    this.addClass(this.skipDiv, this.classes.skipDiv);
    this.skipDiv.innerHTML = this.default.skipLabel;
    this.skipDiv.style.display = 'none';
    this.skipDiv.addEventListener(
      'click',
      this.onSkipAd.bind(this),
      false
    );

    this.assignControlAttributes(this.clickCoverDiv, this.classes.clickCoverDiv);
    this.addClass(this.clickCoverDiv, this.classes.clickCoverDiv);

    this.assignControlAttributes(this.seekBarDiv, 'corejs-seek-bar-div');
    this.seekBarDiv.style.width = '100%';

    this.assignControlAttributes(this.progressDiv, 'corejs-progress-div');

    this.assignControlAttributes(this.playPauseDiv, 'corejs-play-pause-div');
    if (this.isMobile) {
      this.addClass(this.playPauseDiv, this.classes.playPauseDivForMobile);
    }
    this.addClass(this.playPauseDiv, this.classes.playPauseDivStatusPlaying);
    this.playPauseDiv.addEventListener(
      'click',
      this.onAdPlayPauseClick.bind(this),
      false
    );

    this.assignControlAttributes(this.muteDiv, 'corejs-mute-div');
    if (this.isMobile) {
      this.addClass(this.muteDiv, this.classes.muteDivForMobile);
    }
    if (!this.player.muted()) {
      this.addClass(this.muteDiv, this.classes.muteDivStatusUnmute);
    } else {
      this.addClass(this.muteDiv, this.classes.muteDivStatusMuted);
    }
    this.muteDiv.addEventListener(
      'click',
      this.onAdMuteClick.bind(this),
      false
    );

    this.assignControlAttributes(this.sliderDiv, 'corejs-slider-div');
    this.sliderDiv.addEventListener(
      'mousedown',
      this.onAdVolumeSliderMouseDown.bind(this),
      false
    );

    // Hide volume slider controls on iOS as they aren't supported.
    if (this.isIos) {
      this.sliderDiv.style.display = 'none';
    }

    this.assignControlAttributes(this.sliderLevelDiv, 'corejs-slider-level-div');

    this.assignControlAttributes(this.fullscreenDiv, 'corejs-fullscreen-div');
    this.addClass(this.fullscreenDiv, this.classes.fullscreenStatusNonFullScreen);
    this.fullscreenDiv.addEventListener(
      'click',
      this.onAdFullscreenClick.bind(this),
      false
    );

    this.adContainerDiv.appendChild(this.skipDiv);
    this.adContainerDiv.appendChild(this.controlsDiv);
    this.controlsDiv.appendChild(this.countdownDiv);
    this.controlsDiv.appendChild(this.seekBarDiv);
    if (this.isMobile) {
      this.adContainerDiv.appendChild(this.playPauseDiv);
      this.adContainerDiv.appendChild(this.muteDiv);
      this.sliderDiv.style.right = '0.33em';
    } else {
      this.controlsDiv.appendChild(this.playPauseDiv);
      this.controlsDiv.appendChild(this.muteDiv);
      this.controlsDiv.appendChild(this.fullscreenDiv);
    }
    this.controlsDiv.appendChild(this.sliderDiv);
    this.seekBarDiv.appendChild(this.progressDiv);
    this.sliderDiv.appendChild(this.sliderLevelDiv);
    this.adContainerDiv.appendChild(this.skipAd);
    this.adContainerDiv.appendChild(this.clickCoverDiv);
    this.adContainerDiv.appendChild(this.logoDiv);
  }

  onAutoPlayError() {
    this.showBackupImage();
  }

  showBackupImage() {
    if (!this.player.options().ads.backupImg) {
      return;
    }
    this.addClass(this.imgBackup, 'corejs-img-backup');
    this.imgBackup.style.display = 'block';
    this.imgBackup.style.width = '100%';
    this.imgBackup.src = this.player.options().ads.backupImg;
    this.adContainerDiv.appendChild(this.imgBackup);
  }

  hideBackupImage() {
    this.imgBackup.style.display = 'none';
  }

  /**
 * Listener for clicks on the play/pause button during ad playback.
 */
  onAdPlayPauseClick() {
    this.adPlayPauseClickHandler();
  }

  registerAdPlayPauseClickHandler(handler) {
    this.adPlayPauseClickHandler = handler;
  }

  /**
 * Listen for ad skip button available
 */
  onSkipAd() {
  console.log('skipads'); // eslint-disable-line
    this.adManager.skipAd();
  }

  /**
 * Listener for clicks on the play/pause button during ad playback.
 */
  onAdMuteClick() {
    this.adMuteClickHandler();
  }

  registerAdMuteClickHandler(handler) {
    this.adMuteClickHandler = handler;
  }
  /**
 * Listener for clicks on the fullscreen button during ad playback.
 */
  onAdFullscreenClick() {
    this.adFullScreenClickHandler();
  }

  registerAdFullScreenClick(handler) {
    this.adFullScreenClickHandler = handler;
  }

  /**
 * Show pause and hide play button
 */
  onAdsPaused() {
    this.addClass(this.playPauseDiv, this.classes.playPauseDivStatusPause);
    this.removeClass(this.playPauseDiv, this.classes.playPauseDivStatusPlaying);
    this.showAdControls();
  }

  /**
 * Show pause and hide play button
 */
  onAdsResumed() {
    this.onAdsPlaying();
    this.showAdControls();
  }

  /**
 * Show play and hide pause button
 */
  onAdsPlaying() {
    this.addClass(this.playPauseDiv, this.classes.playPauseDivStatusPlaying);
    this.removeClass(this.playPauseDiv, this.classes.playPauseDivStatusPause);
  }

  /**
 * Takes data from the controller to update the UI.
 *
 * @param {number} currentTime Current time of the ad.
 * @param {number} remainingTime Remaining time of the ad.
 * @param {number} duration Duration of the ad.
 * @param {number} adPosition Index of the ad in the pod.
 * @param {number} totalAds Total number of ads in the pod.
 */
  updateAdUi(currentTime, remainingTime, duration, adPosition, totalAds) {
    // Update countdown timer data
    const remainingMinutes = Math.floor(remainingTime / 60);
    let remainingSeconds = Math.floor(remainingTime % 60);

    if (remainingSeconds.toString().length < 2) {
      remainingSeconds = '0' + remainingSeconds;
    }
    let podCount = ': ';

    if (totalAds > 1) {
      podCount = ' (' + adPosition + ' ' +
        this.default.adLabelNofN + ' ' + totalAds + '): ';
    }

    if (remainingTime > 0) {
      this.countdownDiv.innerHTML =
      this.default.adLabel + podCount +
      remainingMinutes + ':' + remainingSeconds;
    } else { // hide countdown and seekbar after play done.
      this.countdownDiv.style.display = 'none';
      this.seekBarDiv.style.display = 'none';
    }

    // Update UI
    const playProgressRatio = currentTime / duration;
    const playProgressPercent = playProgressRatio * 100;
    // const increasePercent = playProgressPercent - currentPercent;

    // const wave = 0.5;
    // const increateProcess = setInterval(() => {
    //   let currentPercent = parseFloat(this.progressDiv.style.width.replace('%', ''), 10) || 0;

    //   if (currentPercent - playProgressPercent > 50) {
    //     currentPercent = 0;
    //   }
    //   this.progressDiv.style.width = (currentPercent + wave) + '%';
    //   if (currentPercent > playProgressPercent) {
    //     clearInterval(increateProcess);
    //   }
    // }, 100);
    this.progressDiv.style.width = playProgressPercent + '%';
  }

  /**
 * Handles UI changes when the ad is unmuted.
 */
  unmute() {
    this.addClass(this.muteDiv, this.classes.muteDivStatusUnmute);
    this.removeClass(this.muteDiv, this.classes.muteDivStatusMuted);
    this.sliderLevelDiv.style.width =
      this.player.volume() * 100 + '%';
  }

  /**
 * Handles UI changes when the ad is muted.
 */
  mute() {
    this.addClass(this.muteDiv, this.classes.muteDivStatusMuted);
    this.removeClass(this.muteDiv, this.classes.muteDivStatusUnmute);
    this.sliderLevelDiv.style.width = '0%';
  }

  /*
 * Listener for mouse down events during ad playback. Used for volume.
 */
  onAdVolumeSliderMouseDown() {
    document.addEventListener('mouseup', this.boundOnMouseUp, false);
    document.addEventListener('mousemove', this.boundOnMouseMove, false);
  }

  /*
 * Mouse movement listener used for volume slider.
 */
  onMouseMove(event) {
    this.changeVolume(event);
  }

  /*
 * Mouse release listener used for volume slider.
 */
  onMouseUp(event) {
    this.changeVolume(event);
    document.removeEventListener('mouseup', this.boundOnMouseUp);
    document.removeEventListener('mousemove', this.boundOnMouseMove);
    console.log('onmouse up'); // eslint-disable-line
  }

  /*
 * Utility function to set volume and associated UI
 */
  changeVolume(event) {
    let percent =
      (event.clientX - this.sliderDiv.getBoundingClientRect().left) /
          this.sliderDiv.offsetWidth;

    percent *= 100;
    // Bounds value 0-100 if mouse is outside slider region.
    percent = Math.min(Math.max(percent, 0), 100);
    this.sliderLevelDiv.style.width = percent + '%';
    if (percent === 0) {
      this.addClass(this.muteDiv, this.classes.muteDivStatusMuted);
      this.removeClass(this.muteDiv, this.classes.muteDivStatusUnmute);
    } else {
      this.addClass(this.muteDiv, this.classes.muteDivStatusUnmute);
      this.removeClass(this.muteDiv, this.classes.muteDivStatusMuted);
      this.muteAction(false);
    }
    this.changeVolumeAction(percent / 100); // 0-1
  }

  registerMuteAction(action) {
    this.muteAction = action;
  }

  registerChangeVolumeAction(action) {
    this.changeVolumeAction = action;
  }

  /**
 * Show the ad container.
 */
  showAdContainer() {
    this.adContainerDiv.style.display = 'block';
  }

  /**
 * Hide the ad container
 */
  hideAdContainer() {
    this.adContainerDiv.style.display = 'none';
  }

  /**
 * Resets the state of the ad ui.
 */
  reset() {
    this.hideAdContainer();
  }

  /**
 * Handles ad errors.
 */
  onAdError() {
    this.showBackupImage();
  }

  /**
 * Handles ad break starting.
 *
 * @param {Object} adEvent The event fired by the IMA SDK.
 */
  onAdBreakStart(ad) {
    this.showAdContainer();

    const contentType = ad.getContentType();

    if ((contentType === 'application/javascript') &&
      !this.player.options().showControlsForJSAds) {
      this.controlsDiv.style.display = 'none';
    } else {
      this.controlsDiv.style.display = 'block';
    }
    this.onAdsPlaying();
    // Start with the ad controls minimized.
    this.hideAdControls();
  }

  /**
 * Handles ad break ending.
 */
  onAdBreakEnd() {
    const currentAd = this.controller.getCurrentAd();

    if (currentAd === null || // hide for post-roll only playlist
      currentAd.isLinear()) { // don't hide for non-linear ads
      this.hideAdContainer();
    }
    this.controlsDiv.style.display = 'none';
    this.countdownDiv.innerHTML = '';
    this.hideAdControls();
  }

  /**
 * Handles when all ads have finished playing.
 */
  onAllAdsCompleted() {
    this.hideAdContainer();
  }

  /**
 * Handles when a linear ad starts.
 */
  onLinearAdStart() {
  // Don't bump container when controls are shown
    this.removeClass(this.adContainerDiv, 'bumpable-corejs-ad-container');
  }

  /**
 * Handles when a non-linear ad starts.
 */
  onNonLinearAdLoad() {
  // For non-linear ads that show after a linear ad. For linear ads, we show the
  // ad container in onAdBreakStart to prevent blinking in pods.
    this.adContainerDiv.style.display = 'block';
    // Bump container when controls are shown
    this.addClass(this.adContainerDiv, 'bumpable-corejs-ad-container');
  }

  onPlayerEnterFullscreen() {
    this.addClass(this.fullscreenDiv, this.classes.fullscreenStatusFullScreen);
    this.removeClass(this.fullscreenDiv, this.classes.fullscreenStatusNonFullScreen);
  }

  onPlayerExitFullscreen() {
    this.addClass(this.fullscreenDiv, this.classes.fullscreenStatusNonFullScreen);
    this.removeClass(this.fullscreenDiv, this.classes.fullscreenStatusFullScreen);
  }

  /**
 * Called when the player volume changes.
 *
 * @param {number} volume The new player volume.
 */
  onPlayerVolumeChanged(volume) {
    if (volume == 0) {
      this.addClass(this.muteDiv, this.classes.muteDivStatusMuted);
      this.removeClass(this.muteDiv, this.classes.muteDivStatusUnmute);
      this.sliderLevelDiv.style.width = '0%';
    } else {
      this.addClass(this.muteDiv, this.classes.muteDivStatusUnmute);
      this.removeClass(this.muteDiv, this.classes.muteDivStatusMuted);
      this.sliderLevelDiv.style.width = volume * 100 + '%';
    }
  }

  /**
 * Shows ad controls on mouseover.
 */
  showAdControls() {
    if (this.isMobile) {
      return;
    }
    const disableAdControls = this.player.options().disableAdControls || this.default.disableAdControls;

    if (!disableAdControls) {
      this.addClass(this.controlsDiv, 'corejs-controls-div-showing');
    }
  }

  /**
 * Hide the ad controls.
 */
  hideAdControls() {
    this.removeClass(this.controlsDiv, 'corejs-controls-div-showing');
  }

  hideControlButton() {
    this.playPauseDiv.style.display = 'none';
    this.muteDiv.style.display = 'none';
    this.fullscreenDiv.style.display = 'none';
  }

  showSkipAd() {
    const disableSkipAdButton = this.player.options().ads.disableSkipAdButton || this.default.disableSkipAdButton;

    if (!disableSkipAdButton) {
      this.skipDiv.style.display = '';
    }
  }

  hideSkipAd() {
    this.skipDiv.style.display = 'none';
  }

  /**
 * Assigns the unique id and class names to the given element as well as the
 * style class.
 *
 * @param {HTMLElement} element Element that needs the controlName assigned.
 * @param {string} controlName Control name to assign.
 */
  assignControlAttributes(element, controlName) {
    element.id = this.controlPrefix + controlName;
    element.className = this.controlPrefix + controlName + ' ' + controlName;
  }

  /**
 * Returns a regular expression to test a string for the given className.
 *
 * @param {string} className The name of the class.
 * @return {RegExp} The regular expression used to test for that class.
 */
  getClassRegexp(className) {
  // Matches on
  // (beginning of string OR NOT word char)
  // classname
  // (negative lookahead word char OR end of string)
    return new RegExp('(^|[^A-Za-z-])' + className +
      '((?![A-Za-z-])|$)', 'gi');
  }

  /**
 * Returns whether or not the provided element has the provied class in its
 * className.
 *
 * @param {HTMLElement} element Element to tes.t
 * @param {string} className Class to look for.
 * @return {boolean} True if element has className in class list. False
 *     otherwise.
 */
  elementHasClass(element, className) {
    const classRegexp = this.getClassRegexp(className);

    return classRegexp.test(element.className);
  }

  /**
 * Adds a class to the given element if it doesn't already have the class
 *
 * @param {HTMLElement} element Element to which the class will be added.
 * @param {string} classToAdd Class to add.
 */
  addClass(element, classToAdd) {
    element.className = element.className.trim() + ' ' + classToAdd;
  }

  /**
 * Removes a class from the given element if it has the given class
 *
 * @param {HTMLElement} element Element from which the class will be removed.
 * @param {string} classToRemove Class to remove.
 */
  removeClass(element, classToRemove) {
    const classRegexp = this.getClassRegexp(classToRemove);

    element.className =
      element.className.trim().replace(classRegexp, '');
  }

  /**
 * @return {HTMLElement} The div for the ad container.
 */
  getAdContainerDiv() {
    return this.adContainerDiv;
  }

  /**
 * Changes the flag to show or hide the ad countdown timer.
 *
 * @param {boolean} showCountdownIn Show or hide the countdown timer.
 */
  setShowCountdown(showCountdownIn) {
    this.showCountdown = showCountdownIn;
    this.countdownDiv.style.display = this.showCountdown ? 'block' : 'none';
  }

}

export default AdUi;
