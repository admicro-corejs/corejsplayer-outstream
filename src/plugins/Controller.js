import sdk from '../sdk';
import AdUi from './ad-ui';

export default class Controller {
  constructor(player, adsManager) {
    this.adsManager = adsManager;
    this.adUi = new AdUi(player, adsManager);
    this.player = player;
    this.volumeBeforeMuted = 1;

    this.registerHandlerForAdUi();
    this.trackPlayPause();
    this.trackFullScreen();
    this.trackMute();
  }

  registerHandlerForAdUi() {
    this.adUi.registerAdPlayPauseClickHandler(this.onAdPlayPauseClick.bind(this));
    this.adUi.registerAdMuteClickHandler(this.onAdMuteClick.bind(this));
    this.adUi.registerAdFullScreenClick(this.onAdFullscreenClick.bind(this));
    this.adUi.registerMuteAction((function(isMute) {
      this.player.muted(isMute);
    }).bind(this));
    this.adUi.registerChangeVolumeAction((function(volume) {
      this.player.volume(volume);
    }).bind(this));

  }

  onAdFullscreenClick() {
    if (!this.player.isFullscreen()) {
      this.player.requestFullscreen();
      this.adUi.onPlayerEnterFullscreen();
    } else {
      this.player.exitFullscreen();
      this.adUi.onPlayerExitFullscreen();
    }
  }

  onAdMuteClick() {
    console.log('checkMute', this.player.muted()); // eslint-disable-line
    if (this.player.muted()) {
      this.player.muted(false);
      this.adUi.unmute();
      this.player.volume(this.volumeBeforeMuted);
    } else {
      this.volumeBeforeMuted = this.player.volume();
      this.player.muted(true);
      this.adUi.mute();
    }
  }

  onAdPlayPauseClick() {
    console.log('checkPlayPause', this.player.paused()); // eslint-disable-line
    if (this.player.paused()) {
      this.player.play();
      setTimeout(() => {
        if (this.player.paused()) {
          const videoEl = this.player.el().getElementsByTagName('video')[0];

          if (videoEl) {
            videoEl.play();
          }
        }
      }, 200);
      this.adUi.onAdsPlaying();
    } else {
      this.player.pause();
      this.adUi.onAdsPaused();
    }
  }

  trackPlayPause() {
    this.player.on('play', () => {
      this.adUi.onAdsPlaying();
    });

    this.player.on('pause', () => {
      this.adUi.onAdsPaused();
    });
  }

  trackFullScreen() {
    this.player.on('fullscreenchange', () => {
      if (this.player.isFullscreen()) {
        this.adUi.onPlayerEnterFullscreen();
      } else {
        this.adUi.onPlayerExitFullscreen();
      }
    });
  }

  trackMute() {
    this.player.on('mute', () => {
      if (this.player.muted()) {
        this.adUi.mute();
      } else {
        this.adUi.unmute();
      }
    });
  }

  getPlayerWidth() {
    return this.player.width();
  }

  getPlayerHeight() {
    return this.player.height();
  }

  /**
   * Call when adManager loaded
   *
   * @param {onject} adsManagerLoadedEvent
   */
  onAdsManagerLoaded(adsManagerLoadedEvent) {
    this.adUi.showAdContainer();
    this.player.controlBar.hide();
    // this.adsManager.autoPlay();

    this.adsManager.addEventListener(
      sdk.Event.AdsErrorEvent.Type.AD_ERROR,
      this.onAdError.bind(this)
    );

    this.adsManager.addEventListener(
      'autoplayerror',
      this.onAutoPlayError.bind(this)
    );

    this.adsManager.addEventListener(
      'showbackupimage',
      () => {
        this.adUi.showBackupImage();
      }
    );

    this.adsManager.addEventListener(
      'hidebackupimg',
      this.hideBackupImage.bind(this)
    );

    this.adsManager.addEventListener(
      sdk.Event.AdsEvent.Type.AD_BREAK_READY,
      this.onAdBreakReady.bind(this)
    );

    this.adsManager.addEventListener(
      sdk.Event.AdsEvent.Type.ALL_ADS_COMPLETED,
      this.onAllAdsCompleted.bind(this)
    );

    this.adsManager.addEventListener(
      sdk.Event.AdsEvent.Type.LOADED,
      this.onAdLoaded.bind(this)
    );

    this.adsManager.addEventListener(
      sdk.Event.AdsEvent.Type.STARTED,
      this.onAdStarted.bind(this)
    );

    this.adsManager.addEventListener(
      sdk.Event.AdsEvent.Type.SKIPPED,
      this.onSkipAd.bind(this)
    );

    this.adsManager.addEventListener(
      sdk.Event.AdsEvent.Type.COMPLETE,
      this.onAdComplete.bind(this)
    );

    this.adsManagerLoaded();
  }

  adsManagerLoaded() {}

  onAdError() {
    this.adUi.onAdError();
  }

  onAdBreakReady() {}

  onAllAdsCompleted() {}

  onAdLoaded() {}

  onAdStarted() {}

  onAdComplete() {}

  onSkipAd() {}

  onAutoPlayError() {
    this.adUi.onAutoPlayError();
    this.adUi.hideControlButton();
    this.player.posterImage.hide();
  }

  hideBackupImage() {
    this.adUi.hideBackupImage();
  }

  videoElement() {
    return this.player.el().getElementsByClassName('vjs-tech')[0];
  }
}
