export default class PlayerConfig {
  constructor(data) {
    this.start = data.start || 0;
    this.crossOrigin = data.crossOrigin || '';
    this.playsinline = data.playsinline || true;
    this.customEventName = data.customEventName || 'statechanged';
    this.options = data.options || {};
    this.events = data.events || [];
    this.globalOptions = data.globalOptions || {
      // autoplay: false,
      controls: true,
      // preload: 'auto',
      // fluid: false,
      // muted: false,
      controlBar: {
        remainingTimeDisplay: false,
        playToggle: {},
        progressControl: {},
        fullscreenToggle: {},
        volumeMenuButton: {
          inline: false,
          vertical: true
        }
      },
      techOrder: ['html5'],
      plugins: {}
    };
    this.globalEvents = data.globalEvents || [];
    this.trackList = data.trackList || [];
  }
}
