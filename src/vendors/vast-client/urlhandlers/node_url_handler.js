const uri = require('url');
const fs = require('fs');
const http = require('http');
const https = require('https');
const DOMParser = require('xmldom').DOMParser;

function get(url, options, cb) {
  url = uri.parse(url);
  const httpModule = url.protocol === 'https:' ? https : http;

  if (url.protocol === 'file:') {
    fs.readFile(url.pathname, 'utf8', function(err, data) {
      if (err) {
        return cb(err);
      }
      const xml = new DOMParser().parseFromString(data);

      cb(null, xml);
    });
  } else {
    let timing;
    let data = '';

    const timeoutWrapper = req => () => req.abort();

    const req = httpModule.get(url.href, function(res) {
      res.on('data', function(chunk) {
        data += chunk;
        clearTimeout(timing);
        timing = setTimeout(fn, options.timeout || 120000); // eslint-disable-line
      });
      res.on('end', function() {
        clearTimeout(timing);
        const xml = new DOMParser().parseFromString(data);

        cb(null, xml);
      });
    });

    const fn = timeoutWrapper(req);

    req.on('error', function(err) {
      clearTimeout(timing);
      cb(err);
    });

    timing = setTimeout(fn, options.timeout || 120000);
  }
}

export const nodeURLHandler = {
  get
};
