export default class ImpressionURLTemplate {
  constructor(data) {
    this.url = data.url;
    this.id = data.id;
  }
}
