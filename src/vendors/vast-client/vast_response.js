export class VASTResponse {
  constructor() {
    this.ads = [];
    this.errorURLTemplates = [];
    this.xml = null;
  }
}
