function Subscriber() {
  this._subscribers = {};
}

Subscriber.prototype.subscribe = function subscribe(handler, eventName, context) {
  if (!this.isHandlerAttached(handler, eventName)) {
    this.get(eventName).push({handler, context, eventName});
  }
};

Subscriber.prototype.unsubscribe = function unsubscribe(handler, eventName) {
  this._subscribers[eventName] = this.get(eventName).filter(function(subscriber) {
    return handler !== subscriber.handler;
  });
};

Subscriber.prototype.unsubscribeAll = function unsubscribeAll() {
  this._subscribers = {};
};

Subscriber.prototype.trigger = function(eventName, data) {
  const that = this;
  const subscribers = this.get(eventName)
    .concat(this.get('*'));

  subscribers.forEach(function(subscriber) {
    setTimeout(function() {
      if (that.isHandlerAttached(subscriber.handler, subscriber.eventName)) {
        subscriber.handler.call(subscriber.context, data);
      }
    }, 0);
  });
};

Subscriber.prototype.triggerSync = function(eventName, data) {
  const subscribers = this.get(eventName)
    .concat(this.get('*'));

  subscribers.forEach(function(subscriber) {
    subscriber.handler.call(subscriber.context, data);
  });
};

Subscriber.prototype.get = function get(eventName) {
  if (!this._subscribers[eventName]) {
    this._subscribers[eventName] = [];
  }
  return this._subscribers[eventName];
};

Subscriber.prototype.isHandlerAttached = function isHandlerAttached(handler, eventName) {
  return this.get(eventName).some(function(subscriber) {
    return handler === subscriber.handler;
  });
};

export default Subscriber;

