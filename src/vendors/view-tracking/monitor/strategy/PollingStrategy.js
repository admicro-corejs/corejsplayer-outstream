/**
 * @typedef {Object} PollingStrategyConfig
 * @name PollingStrategyConfig
 * @memberof ViewTracking.VisMon.Strategy.PollingStrategy#
 *
 * @property {number} [interval=5000] The interval between state updates
 * in milliseconds.
 *
 * @description A configuration object to configure a PollingStrategy instance.
 */

/**
 * @class
 * @name PollingStrategy
 * @extends ViewTracking.VisMon.Strategy
 * @memberof ViewTracking.VisMon.Strategy
 *
 * @param {ViewTracking.VisMon.Strategy.PollingStrategy#PollingStrategyConfig} [config={interval:1000}] The config object
 *
 * @property {ViewTracking.VisMon.Strategy.PollingStrategy#PollingStrategyConfig} _config The internal config object
 *
 * @classdesc A strategy that will periodically update the objects
 * visibility state.
 *
 * @example
 *
 * var visMon = ViewTracking(...).monitor({
 *   strategy: new ViewTracking.VisMon.Strategy.PollingStrategy({
 *     interval: 5000
 *   }),
 *   update: function() {
 *     console.log('updated.');
 *   }
 * }).start();
 *
 */

import StrategyBase from './StrategyBase';
import defaults from '../../util/defaults';

export default class PollingStrategy extends StrategyBase {
  constructor(config) {
    super();
    this._config = defaults(config, {
      interval: 1000
    });
    this._started = false;
  }

  start(monitor) {
    if (!this._started) {
      const intervalId = setInterval(() => monitor.update(), this._config.interval);

      this._clearInterval = () => clearInterval(intervalId);

      this._started = true;
    }

    return this._started;
  }

  stop() {
    if (!this._started) {
      return false;
    }

    this._clearInterval();

    this._started = false;

    return true;
  }
}
