import StrategyBase from './StrategyBase';
import isArray from '../../util/isArray';
import isFunction from '../../util/isFunction';

/**
 * @class
 * @name CompositeStrategy
 * @extends ViewTracking.VisMon.Strategy
 * @memberof ViewTracking.VisMon.Strategy
 *
 * @param {ViewTracking.VisMon.Strategy[]} strategies
 *
 * @property {ViewTracking.VisMon.Strategy[]} _strategies A list of strategies
 *
 * @classdesc A composite strategy to combine two or more strategies
 * Its a proxy that will call every strategies start() and stop() methods.
 *
 * @example
 *
 * var visMon = ViewTracking(...).monitor({
 *   strategy: new ViewTracking.VisMon.Strategy.CompositeStrategy([
 *      new ViewTracking.VisMon.Strategy.EventStrategy(...),
 *      new ViewTracking.VisMon.Strategy.PollingStrategy(...)
 *   ]),
 *   update: () => console.log('updated')
 * }).start();
 *
 */
export default class CompositeStrategy extends StrategyBase {
  constructor(strategies) {
    super();
    this._strategies = !strategies ? [] : (isArray(strategies) ? strategies : [strategies]); // eslint-disable-line
  }

  init(monitor) {
    this._strategies
      .filter(strategy => isFunction(strategy.init))
      .forEach(strategy => strategy.init(monitor));
  }

  start(monitor) {
    this._strategies
      .filter(strategy => isFunction(strategy.start))
      .forEach(strategy => strategy.start(monitor));
  }

  stop(monitor) {
    this._strategies
      .filter(strategy => isFunction(strategy.stop))
      .forEach(strategy => strategy.stop(monitor));
  }
}
