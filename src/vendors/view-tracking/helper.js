import EventRegistry from './helper/EventRegistry';
import PubSub from './helper/PubSub';

export default () => ({
  EventRegistry,
  PubSub
});
