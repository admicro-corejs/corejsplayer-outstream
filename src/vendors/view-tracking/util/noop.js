/**
 * @function
 * @name noop
 * @memberof twynUtils
 *
 * @return undefined
 *
 * @description A no-operation function.
 *
 * @example
 *
 * var object = { 'name': 'Bradley' };
 * twynUtils.noop(object) === undefined;
 * // => true
 *
 */
export default function noop() {
}
