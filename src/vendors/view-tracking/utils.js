import _async from './util/async';
import debounce from './util/debounce';
import defaults from './util/defaults';
import defer from './util/defer';
import extend from './util/extend';
import identity from './util/identity';
import isArray from './util/isArray';
import isDefined from './util/isDefined';
import isElement from './util/isElement';
import isFriendlyIframeContext from './util/isFriendlyIframeContext';
import isFunction from './util/isFunction';
import isIframeContext from './util/isIframeContext';
import isObject from './util/isObject';
import noop from './util/noop';
import now from './util/now';
import once from './util/once';
import forEach from './util/forEach';
import throttle from './util/throttle';

import * as _elementFunctions from './util/_element';

const percentage = _elementFunctions.percentage;
const viewport = _elementFunctions.viewport;
const isInViewport = _elementFunctions.isInViewport;
const isDisplayed = _elementFunctions.isDisplayed;
const styleProperty = _elementFunctions.styleProperty;
const computedStyle = _elementFunctions.computedStyle;
const isVisibleByStyling = _elementFunctions.isVisibleByStyling;
const isPageVisible = _elementFunctions.isPageVisible;
const createVisibilityApi = _elementFunctions.createVisibilityApi;

export default {
  async: _async,
  debounce,
  defaults,
  defer,
  extend,
  forEach,
  identity,
  isArray,
  isDefined,
  isElement,
  isFriendlyIframeContext,
  isFunction,
  isIframeContext,
  isObject,
  noop,
  now,
  once,
  throttle,

  /**
     * backward compatibility to <1.0.0
     * */
  VisibilityApi: createVisibilityApi(),
  createVisibilityApi,
  isPageVisible,
  isVisibleByStyling,
  percentage,
  _viewport: viewport,
  _isInViewport: isInViewport,

  _isDisplayed: isDisplayed,

  _computedStyle: computedStyle,
  _styleProperty: styleProperty
};
