import Event from './event';

function generate() {
  const baseConfig = [
    new Event('play', (name, callback, type, value) => {
      console.log('play'); // eslint-disable-line
    }, 'normal'),
    new Event('pause', (name, callback, type, value) => {
      console.log('pause'); // eslint-disable-line
    }, 'normal'),
    new Event('skip', (name, callback, type, value) => {
      console.log('skip'); // eslint-disable-line
    }, 'normal')
  ];

  return baseConfig;
}

export default generate;
