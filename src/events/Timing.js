import Event from './event';

function generate() {
  const baseconfig = [
    new Event('timeupdate', (name, callback, type, value) => {
      console.log('25%'); // eslint-disable-line
    }, 'percent', 0.25),
    new Event('timeupdate', (name, callback, type, value) => {
      console.log('50%'); // eslint-disable-line
    }, 'percent', 0.5),
    new Event('timeupdate', (name, callback, type, value) => {
      console.log('75%'); // eslint-disable-line
    }, 'percent', 0.75),
    new Event('timeupdate', (name, callback, type, value) => {
      console.log('100%'); // eslint-disable-line
    }, 'percent', 1)
  ];

  return baseconfig;
}

export default generate;

// export default class Timing {
//   constructor(config) {
//     const copyBase = baseconfig.map(i => i);

//     this.config = Array.isArray(config) ? config.concat(copyBase) : copyBase;
//   }

//   register(percent, callback) {
//     this.config.push({
//       percent: percent,
//       do: callback,
//     });
//   }

//   do(player) {
//     const current = this.player.currentTime() / this.player.duration();
//     const error = 0.1;
//     const handlerFilter = this.config.filter(handler => ((handler.percent === current || current > handler.percent) && current < (handler.percent + error)));

//     for (let index = 0; index < handlerFilter.length; index++) {
//       const handler = handlerFilter[index];

//       try {
//         handler.do(player);
//       } catch (error) {
//         //
//       }

//       this.config.splice(index, 1);
//     }
//   }
// }
