export default class Event {
  constructor(name, callback, type, value) {
    this.name = name;
    this.callback = callback;
    this.value = value;
    this.type = type || 'normal';
    this.done = false;
  }

  invokeCallback(name, value, type) {
    if (this.callback instanceof Function) {
      try {
        this.callback(name, value, type);
      } catch (error) {
        console.error(error); // eslint-disable-line
      }
    }
    if (this.name === 'timeupdate' && (this.type === 'heartbeat' || this.type === 'continuous')) {
      this.done = false;
    } else {
      this.done = true;
    }
  }

  isValid() {
    return this.callback instanceof Function && this.name && this.value;
  }
}
