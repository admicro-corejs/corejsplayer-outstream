import Event from './event';
import timing from './Timing';
import interactive from './interactive';

class EventHandler {
  constructor() {
    this.events = [];
    this.heartbeat = null;
    this.events = this.events
      .concat(timing())
      .concat(interactive());
  }

  register(name, callback, type, value) {
    const event = new Event(name, callback, type, value);

    if (!event.isValid) {
      throw new Error('invalid event.');
    }
    this.events.push(event);
  }

  handler(currentEvent, adsManager, player, value) {
    const getEvents = this.events.filter(e => e.name === currentEvent);

    for (let index = 0; index < getEvents.length; index++) {
      const event = getEvents[index];
      const currentTime = player.currentTime() || adsManager.currentTime;

      try {
        if (event.name === 'timeupdate' || event.name === 'tick') {
          const current = event.type === 'percent' ? currentTime / player.duration() : currentTime;

          if (event.type === 'heartbeat') {
            const currentBeat = parseInt(current, 10);

            if (currentBeat !== this.heartbeat) {
              this.heartbeat = currentBeat;
              event.invokeCallback(currentEvent, currentBeat, event.type);
            }
          } else if ((!event.done || event.type === 'continuous') &&
            (event.value === current || current > event.value)) {
            event.invokeCallback(currentEvent, value, event.type);
          }
        } else {
          event.invokeCallback(currentEvent, value, event.type);
        }
      } catch (error) {
        console.log(error); // eslint-disable-line
      }
    }
    // if (this.events.filter(e => e.name === currentEvent).length === 0) console.log(currentEvent); // eslint-disable-line
  }
}

export default EventHandler;
