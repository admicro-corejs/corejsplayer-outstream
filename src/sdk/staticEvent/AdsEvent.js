/* eslint-disable no-inline-comments */

export default {
  Type: {
    TRUE_VIEW: 'trueview',
    CONTENT_RESUME_REQUESTED: 'contentResumeRequested',
    CONTENT_PAUSE_REQUESTED: 'contentPauseRequested', // dừnd nội dung chính đang chạy, do load quảng cáo
    CLICK: 'click',
    DURATION_CHANGE: 'durationChange',
    EXPANDED_CHANGED: 'expandedChanged',
    STARTED: 'start',
    IMPRESSION: 'impression',
    PAUSED: 'pause',
    RESUMED: 'resume',
    AD_REQUEST: 'adRequest',
    AD_PROGRESS: 'adProgress',
    AD_BUFFERING: 'adBuffering',
    FIRST_QUARTILE: 'firstQuartile',
    MIDPOINT: 'midpoint',
    THIRD_QUARTILE: 'thirdQuartile',
    COMPLETE: 'complete',
    USER_CLOSE: 'userClose',
    LINEAR_CHANGED: 'linearChanged',
    LOADED: 'loaded',
    AD_CAN_PLAY: 'adCanPlay',
    AD_METADATA: 'adMetadata',
    AD_BREAK_READY: 'adBreakReady',
    INTERACTION: 'interaction',
    ALL_ADS_COMPLETED: 'allAdsCompleted',
    SKIPPED: 'skip',
    SKIPPABLE_STATE_CHANGED: 'skippableStateChanged',
    LOG: 'log',
    VIEWABLE_IMPRESSION: 'viewable_impression',
    VOLUME_CHANGED: 'volumeChange',
    VOLUME_MUTED: 'mute'
  }
};
