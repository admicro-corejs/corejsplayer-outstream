import { VASTClient, VASTParser } from '../vendors/vast-client';
import window from 'global/window';
import Setting from './Setting';
import AdsError from './AdsError';
import Event from './staticEvent';
import AdsManagerLoadedEvent from './AdsManagerLoadedEvent';
import { EventEmitter } from 'events';
import AdsManager from './AdsManager';
import debuger from './AdDebug';

export default class AdsLoader extends EventEmitter {
  constructor(player) {
    super();
    this.vastClient = new VASTClient();
    this.vASTParser = new VASTParser();
    this.adsRequest = null;
    this.ads = null;
    this.settings = new Setting();
    this.player = player;
    this.eventslistener = [];
    this.adOptions = null;
    this.xml = null;
  }

  setPlayer(player) {
    this.player = player;
  }

  requestAds(adsRequest, adOptions) {
    this.adsRequest = adsRequest;
    this.adOptions = adOptions;
    debuger.adRequest();

    if (adsRequest.adTagUrl) {
      this.vastClient.get(adsRequest.adTagUrl)
        .then((response) => {
          this.handleVast(response);
        })
        .catch((error) => {
          this.handleError(error);
        });
    } else if (adsRequest.vastXml) {
      const xmlParser = new window.DOMParser();
      const xmlDoc = xmlParser.parseFromString(adsRequest.vastXml, 'text/xml');

      this.vASTParser.parseVAST(xmlDoc)
        .then((response) => {
          this.handleVast(response);
        })
        .catch((error) => {
          this.handleError(error);
        });
    }
  }

  handleVast(response) {
    const ads = response.ads;

    this.xml = response.xml;

    if (Array.isArray(ads) && ads.length === 0) {
      debuger.adError('Vast response empty ad.');
      this.emit(Event.AdsErrorEvent.Type.AD_ERROR, 'Vast response empty ad.');
      this.emit(Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, new Error('Vast response empty ad.'));
      return;
    }
    this.ads = ads;

    this.emit(Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED);

    debuger.adLoaded();
  }

  handleError(error) {
    this.emit(Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, error);
    console.log(error); // eslint-disable-line
    debuger.adError(new AdsError(error, 'Cannot request Ads.'));
    this.emit(Event.AdsErrorEvent.Type.AD_ERROR, new AdsError(error, 'Cannot request Ads.'));
  }

  getSettings() {
    return this.settings;
  }

  contentComplete() {}

  addEventListener(event, callback) {
    const self = this;

    if (event === Event.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED) {
      this.on(event, (err) => {
        const adsManager = new AdsManager(self, err);

        callback(new AdsManagerLoadedEvent(adsManager));
      });
    } else {
      this.on(event, callback);
    }
  }
}
