export default class EventHandler {
  constructor(player) {
    this.player = player;
  }

  addEventListener(event, callback) {
    this.player.on(event, callback);
  }

  trigger(event, value) {
    this.player.trigger(event, value);
  }
}
