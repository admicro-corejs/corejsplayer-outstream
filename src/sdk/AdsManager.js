/* eslint-disable no-inline-comments */
import window from 'global/window';
import { VASTTracker } from '../vendors/vast-client';
// import VPAIDHTML5Client from '../vendors/vpaid-client/VPAIDHTML5Client';
// import VPAIDHTML5Tech from '../vendors/vpaid-tech/VPAIDHTML5Tech';
import VPAIDIntegrator from '../vendors/vpaid-patch/ads/vpaid/VPAIDIntegrator';
import VastClient from '../vendors/vpaid-patch/ads/vast/VASTClient';
import AdsEvent from './staticEvent/AdsEvent';
import AdEvent from './models/AdEvent';
import AdsErrorEvent from './staticEvent/AdsErrorEvent';
import viewTracking from '../vendors/view-tracking/entry';
// import canAutoPlay from '../vendors/can-autoplay';
import Creative from './models/Creative';
// import async from '../vendors/vpaid-tech/utils/async';
// import VASTError from '../vendors/vpaid-tech/VASTError';
import { EventEmitter } from 'events';
import debuger from './AdDebug';
import sdk from '.';

export default class AdsManager extends EventEmitter {
  constructor(adsLoader, err) {
    super();
    this.adsLoader = adsLoader;
    this.adUi = null;
    this.ads = [];
    this.vastTracker = null;
    this.adSource = [];
    this.timeToSkip = 1; // default value
    this.isShowSkipAd = false;
    this.currentAds = null;
    this.currentCreative = null;
    this.visibility = viewTracking(this.adsLoader.player.el());
    this.snapshotPlayStatus = null;
    this.currentTime = 0;
    this.tickTimer = null;
    this.isAutoPlayError = false;
    this.isAdError = false;
    this.loadErr = err;
  }

  init(playerWidth, playerHeight) {
    this.resize(playerWidth, playerHeight);
  }

  viewable(callback, customPercent, customTimeIn, destroy = true) {
    const percent = this.adsLoader.adsRequest.viewable.percent || customPercent || 0.5;
    const timeIn = this.adsLoader.adsRequest.viewable.timeIn || customTimeIn || 2000;

    this.visibility.onPercentageTimeTestPassed((monitor) => {
      if (callback) {
        callback();
      }
      if (destroy) {
        monitor.stop();
      }
    }, {
      percentageLimit: percent,
      timeLimit: timeIn,
      interval: 50
    });
  }

  /**
   * set volumm of ad
   *
   * @param {number} playerVolumm
   */
  setVolume(playerVolumm) {
    this.adsLoader.player.volume(playerVolumm);
    debuger.volummChange(playerVolumm, this.getCurrentTime());
  }

  /**
   * return remaining time of ads
   */
  getRemainingTime() {
    return (this.getVideoDuration() || this.currentCreative.getDuration()) - this.getCurrentTime();
  }

  getCurrentTime() {
    // return this.isAutoPlayError ? this.currentTime : this.adsLoader.player.currentTime();
    return this.adsLoader.player.currentTime();
  }
  // List of time offsets from the beginning of the content at which ad breaks are scheduled
  getCuePoints() {}

  /* interactive events */
  // start run ads from preroll
  start() {
    this.emit('showbackupimage');
    if (this.vastTracker) {
      this.vastTracker.setPaused(false);
    }
    this.adsLoader.player.on('ended', () => {
      debuger.adEnd(this.getCurrentTime());
      this.trackerInterval(true); // force full progress bar if complete video
      this.emit('showbackupimage');
    });

    this.adsLoader.player.on('play', () => {
      this.emit('hidebackupimg');
    });

    this.adsLoader.player.on('error', () => {
      // eslint-disable-next-line
      this.emit(sdk.Event.AdsErrorEvent.Type.AD_ERROR);
      setTimeout(() => {
        this.adsEnded.bind(this)();
      }, 10000);
    });

    this.adsLoader.player.on('loadstart', () => {
      this.adsLoader.player.play()
        .then(() => {
          this.emit('hidebackupimg');
        })
        .catch((error) => {
          this.isAutoPlayError = true;
          this.handleAutoPlayError(error);
        });
      // this.isAutoPlayError = true;
      // this.handleAutoPlayError();
    });

    // retry if network error
    let retryNetwork;

    this.adsLoader.player.on('error', () => {
      retryNetwork = setInterval(() => {
        const time = this.getCurrentTime();

        if (this.adsLoader.player.error() && [2, 4].includes(this.adsLoader.player.error().code)) {
          console.log('retry network'); // eslint-disable-line
          // this.adsLoader.player.error(null);
          this.adsLoader.player.pause();
          this.adsLoader.player.load();
          this.adsLoader.player.currentTime(time);
          this.adsLoader.player.play();
        }
      }, 2000);
    });
    setTimeout(() => {
      clearInterval(retryNetwork);
    }, 10000);
    this.adsLoader.player.on('playing', () => {
      console.log('remove interval retry network'); // eslint-disable-line
      clearInterval(retryNetwork);
    });
    // this.emit(AdsEvent.Type.STARTED); this event's emmited in "this.playAd(this.adsLoader.ads)";
    this.playAd(this.adsLoader.ads);
    this.adsLoader.player.one('play', () => {
      this.onStart(this.currentCreative);
      this.viewable(() => {
        console.log('truevew detected'); // eslint-disable-line
        this.adInTrueView();
      });
      this.viewable(() => {
        // this.adsLoader.player.pause();
        this.adsLoader.player.play();
      }, this.adsLoader.adsRequest.viewable.percent || 0.5, 500, false);
    });
  }

  handleAutoPlayError(error) { // eslint-disable-line
    if (!this.tickTimer) {
      this.tick();
    }
    // this.on('tick', this.trackProgress.bind(this));
    this.on('endtick', this.adsEnded.bind(this));
    setTimeout(() => {
      console.log('fired ad complete event'); // eslint-disable-line
      this.adsEnded.bind(this)();
    }, 10000);
    setTimeout(() => {
      if (!(this.adsLoader.player.error() && [2, 4].includes(this.adsLoader.player.error().code))) {
        this.emit('autoplayerror');
      }
    }, 500);
    this.adUi.showSkipAd();
    debuger.pushEvent('autoplayerror');
  }

  tick() {
    this.tickTimer = setInterval(() => {
      if (this.getRemainingTime() > 0) {
        this.currentTime += 0.1;
        this.emit('tick', this.currentTime);
      } else {
        clearInterval(this.tickTimer);
        this.tickTimer = null;
        this.emit('endtick');
      }
    }, 100);
  }

  disposeTick() {
    if (this.tickTimer) {
      clearInterval(this.tickTimer);
    }
  }

  autoPlay() {
    // const id = this.adsLoader.player.id();

    // this.adsLoader.player.one('canplaythrough', () => {
    //   debuger.pushEvent('inside-canplaythrough', null, JSON.stringify({ paused: this.adsLoader.player.paused(), type: this.curentCreative.type }));
    //   if (this.adsLoader.player.paused() && this.curentCreative.type !== 'application/javascript') {
    //     debuger.adStart('running ads');
    //     setTimeout(() => {
    //       const promise = this.adsLoader.player.play();

    //       if (promise !== undefined) {
    //         promise.catch((error) => {
    //           // Auto-play was prevented
    //           // Show a UI element to let the user manually start playback
    //           debuger.autoPlay(`player ${id} error auto play: ${JSON.stringify(error)}`);
    //         }).then(() => {
    //           // Auto-play started
    //         });
    //       }
    //     }, 500);
    //   }
    // });
    console.log('adOptions', this.adsLoader.adOptions); // eslint-disable-line
    this.emit('showbackupimage');
    if (typeof this.adsLoader.adOptions.autoplay === 'undefined' || this.adsLoader.adOptions.autoplay === true) {
      this.viewable(() => {
        debuger.autoPlay();
        this.start();
      }, 0.5, 500);
    }
  }

  adInTrueView() {
    debuger.trueView(this.getCurrentTime());
    this.emit(AdsEvent.Type.TRUE_VIEW, this.getCurrentTime());
  }

  resize(width, height, viewmode) { // eslint-disable-line
    console.log('resize from AdsManager', { width, height }); // eslint-disable-line
    this.adsLoader.player.width(width);
    this.adsLoader.player.height(height);
  }

  pause() {
    if (!this.vastTracker) {
      return;
    }
    if (this.getCurrentTime() < (this.getVideoDuration() || this.currentCreative.getDuration())) {
      this.vastTracker.setPaused(true);
      this.emit(AdsEvent.Type.PAUSED);
      debuger.adPause(this.getCurrentTime());
    }
  }

  resume() {
    if (!this.vastTracker) {
      return;
    }
    this.vastTracker.setPaused(false);
    this.adsLoader.player.play();
    this.emit(AdsEvent.Type.RESUMED);
    debuger.adResume(this.getCurrentTime());
  }

  click() {
    if (this.vastTracker) {
      this.vastTracker.click();
    } else {
      (this.clickThrough.bind(this))(this.currentCreative.videoClickThroughURLTemplate);
    }
    this.emit(AdsEvent.Type.CLICK, this.currentCreative.videoClickThroughURLTemplate);
    debuger.adClick(this.getCurrentTime());
    if (this.adsLoader.adOptions && !this.adsLoader.adOptions.repeatClick) {
      this.adUi.clickCoverDiv.remove();
    }
  }

  skipAd() {
    if (this.vastTracker) {
      (this.vastTracker.skip || this.vastTracker.trackSkip).bind(this.vastTracker)();
    }

    this.emit(AdsEvent.Type.SKIPPED);
    if (this.vastTracker && this.vastTracker.trackSkip) {
      this.vastTracker.trackComplete();
      this.vastTracker = null;
      this.adUi.onAdsPaused();
      this.emit(AdsEvent.Type.COMPLETE);
    }
    this.adsLoader.player.trigger('ended');
    debuger.skipAd(this.getCurrentTime());
  }

  muted() {
    if (!this.vastTracker) {
      return;
    }
    this.vastTracker.setMuted(true);
    this.emit(AdsEvent.Type.VOLUME_CHANGED);
    this.emit(AdsEvent.Type.VOLUME_MUTED);
    debuger.adMute(this.getCurrentTime());
  }

  unmute() {
    if (!this.vastTracker) {
      return;
    }
    this.vastTracker.setMuted(false);
    this.emit(AdsEvent.Type.VOLUME_CHANGED);
  }

  clickThrough(url) {
    if (!this.adsLoader.adOptions.disableClickRedirect) {
      window.open(url);
    }
  }

  trackerFullscreenCheck() {
    if (this.vastTracker) {
      this.vastTracker.setFullscreen(this.adsLoader.player.isFullscreen());
    }

    // do this trick for ios device, play get pause when leave full screen mode so we execute play(),
    // we must add inside setTimeout because of waiting for player totally exit native player on safari.
    if (!this.adsLoader.player.isFullscreen()) {
      setTimeout(() => {
        if (this.adsLoader.player.paused()) {
          this.adsLoader.player.play();
        }
      }, 500);
    }
  }

  trackerVolumeChange() {
    if (!this.vastTracker) {
      return;
    }
    this.vastTracker.setMuted(this.adsLoader.player.muted());
  }

  trackProgress() {
    if (this.vastTracker) {
      this.vastTracker.setProgress(this.getCurrentTime());
    }

    this.trackerInterval();
  }

  onStart(ad) {
    this.emit(AdsEvent.Type.STARTED, new AdEvent(ad, AdsEvent.Type.STARTED));
    if (!this.tickTimer) {
      this.tick();
    }
    debuger.adStart(this.getCurrentTime());

    // player.on('resize', resizeAd);
    // window.addEventListener('resize', resizeAd);
  }
  /* end */

  fireClose() {
    if (!this.vastTracker) {
      return;
    }
    this.vastTracker.close();
  }

  getPlayerDimensions() {
    let width = this.adsLoader.player.currentWidth();
    let height = this.adsLoader.player.currentHeight();

    if (this.adsLoader.player.isFullscreen()) {
      width = window.innerWidth;
      height = window.innerHeight;
    }

    return {width, height};
  }

  loadJSAdUnit(creative, onloadedData, onAdStarted) {
    // const self = this;
    // const mediaFile = creative.mediaFiles[0];
    // const vpaidTech = new VPAIDHTML5Tech(mediaFile);
    this.adsLoader.player.one('ended', () => {
      this.emit(AdsEvent.Type.COMPLETE);
    });
    const vastClient = new VastClient();
    const vpaidIntergrator = new VPAIDIntegrator(this.adsLoader.player, {
      timeout: 500,
      iosPrerollCancelTimeout: 2000,
      adCancelTimeout: 20000,
      playAdAlways: true,
      adsEnabled: true,
      autoResize: true,
      verbosity: 0,
      containerEl: this.adUi.vpaidContainerDiv
    });

    vpaidIntergrator.setPublicTracker((tracker) => {
      this.vastTracker = tracker;
      this.emit('vasttrackinginitialized');
    });

    vastClient.getVASTResponseFromXml(this.adsLoader.xml, (err1, vastRespone) => {
      console.log('yyyy', err1, vastRespone); // eslint-disable-line

      if (err1) {
        this.handleAutoPlayError(err1);
      } else {
        vpaidIntergrator.playAd(vastRespone, (err2, vastResponse) => {
          console.log('xxxx', err2, vastResponse); // eslint-disable-line
          if (err2) {
            this.handleAutoPlayError(err2);
          }
        });
      }
    });

    // vastClient.getVASTResponse(this.adsLoader.adOptions.adTagUrl, (err, vastRespone) => {
    //   console.log('yyyy', err, vastRespone); // eslint-disable-line
    //   if (err) {
    //     //
    //   } else {
    //     vpaidIntergrator.playAd(vastRespone, (e, r) => {
    //       console.log('xxxx', e, r); // eslint-disable-line
    //     });
    //   }
    // });
  }

  filterAdSource(adSource) {
    if (Array.isArray(adSource)) {
      if (adSource.length === 1) {
        return adSource[0];
      }
      return adSource.filter(a => a.type === 'video/mp4')[0];
    }
    return null;
  }

  playAd(ads) {
    // this.adsLoader.player.one('adended', endOfGroup);
    // this.adsLoader.player.one('aderror', nextAd);
    // this.vastTracker.once(AdsEvent.Type.COMPLETE, endOfGroup);
    if (!ads) {
      return;
    }

    debuger.pushEvent('startFunctionPlayAd', null, ads.length);

    try {
      for (let a = 0; a < ads.length; a++) {
        for (let c = 0; c < ads[a].creatives.length; c++) {
          const creative = ads[a].creatives[c];

          let alternateFiles = false;

          switch (creative.type) {
          case 'linear':
            const adSource = [];

            this.currentAds = ads[a];
            this.emit(sdk.Event.AdsEvent.Type.LOADED, this.currentAds);

            creative.mediaFiles.sort((mediaA, mediaB) => {
              const num1 = mediaA.mimeType === 'video/mp4' ? 1 : 2;
              const num2 = mediaB.mimeType === 'video/mp4' ? 1 : 2;

              if (num1 < num2) {
                return -1;
              }
              if (num1 > num2) {
                return 1;
              }

              return 0;
            });
            for (let m = 0; m < creative.mediaFiles.length; m++) {
              const mediaFile = creative.mediaFiles[m];

              if (mediaFile.apiFramework) {
                if (mediaFile.apiFramework === 'VPAID') {
                  if (mediaFile.mimeType === 'video/mp4') {
                    adSource.push({
                      type: mediaFile.mimeType,
                      src: mediaFile.fileURL,
                      width: mediaFile.width,
                      height: mediaFile.height
                    });
                    continue;
                  } else if (mediaFile.mimeType === 'application/javascript') {
                    adSource.push({
                      type: mediaFile.mimeType,
                      src: mediaFile.fileURL,
                      width: mediaFile.width,
                      height: mediaFile.height
                    });
                    alternateFiles = true;
                    break;
                  } else {
                    console.warn(mediaFile.mimeType, 'is not supported yet'); // eslint-disable-line
                    console.log(mediaFile); // eslint-disable-line
                    continue;
                  }
                } else {
                  console.warn('No support for', mediaFile.apiFramework, 'yet'); // eslint-disable-line
                  console.log(mediaFile); // eslint-disable-line
                  continue;
                }
              }

              adSource.push({
                type: mediaFile.mimeType,
                src: mediaFile.fileURL,
                width: mediaFile.width,
                height: mediaFile.height
              });
            }

            const source = this.filterAdSource(adSource);
            const ad = new Creative(creative, source.type);

            this.currentCreative = ad;
            const currentWidth = this.adsLoader.player.currentWidth();
            const currentHeight = this.adsLoader.player.currentHeight();

            if (source.type !== 'application/javascript') {
              alternateFiles = false;
            }

            if (source) {
              this.adUi.onAdBreakStart(ad);
              if (!alternateFiles) {
                this.initTracker(ads[a], creative, alternateFiles);
                debuger.pushEvent('changeSource', null, source);
                this.adsLoader.player.src(source);
                this.resize(currentWidth, currentHeight);
              } else {
                window.__corejsPlayerAlternateFiles = alternateFiles;
                // this.viewable(() => {
                //   console.log('auto continue'); // eslint-disable-line
                //   this.adsLoader.player.play();
                // }, 0.5, 500, false);
                this.loadJSAdUnit(creative, () => {
                  this.emit(AdsEvent.Type.LOADED, new AdEvent(ad, AdsEvent.Type.LOADED));
                }, () => {
                  this.resize(currentWidth, currentHeight);
                });
              }
            } else {
              console.error('No possible sources for this creative', creative); // eslint-disable-line
              this.emit(AdsErrorEvent.Type.AD_ERROR);
              // this.adsLoader.player.trigger('aderror');
            }
            break;
          case 'non-linear':
          case 'companion':
            break;
          default:
            console.warn("There's some type we don't know about:", creative.type); // eslint-disable-line
            break;
          }
        }
      }
    } catch (error) {
      console.error(error); // eslint-disable-line
    }
  }

  initTracker(ad, creative, alternateFiles) {
    this.vastTracker = new VASTTracker(this.adsLoader.vastClient, ad, creative);
    this.emit('vasttrackinginitialized');
    this.vastTracker.trackImpression();
    this.vastTracker.on('clickthrough', this.clickThrough.bind(this));
    // if (options.skipDelay) {
    //   this.vastTracker.setSkipDelay(options.skipDelay);
    // }

    if (alternateFiles) {
      this.adUi.clickCoverDiv.remove();
    } else {
      this.adUi.clickCoverDiv.addEventListener('click', this.click.bind(this));
    }
    // this.adUi.imgBackup.addEventListener('click', this.click.bind(this));

    // skipButton.addEventListener('click', skipAd);

    this.adsLoader.player.on('timeupdate', this.trackProgress.bind(this));
    this.adsLoader.player.on('volumechange', this.trackerVolumeChange.bind(this));
    this.adsLoader.player.on('pause', this.pause.bind(this));
    this.adsLoader.player.on('fullscreenchange', this.trackerFullscreenCheck.bind(this));
    window.addEventListener('beforeunload', this.fireClose.bind(this));
    this.adsLoader.player.one('ended', this.adsEnded.bind(this)); // also in loadJSAdUnit
  }

  adsEnded() {
    if (this.vastTracker) {
      this.vastTracker.complete();
      this.vastTracker = null;
    }
    // skipButton.removeEventListener('click', skipAd);
    // this.adUi.clickCoverDiv.removeEventListener('click', this.click.bind(this));
    // this.adsLoader.player.off('timeupdate', this.trackProgress.bind(this));
    this.adsLoader.player.off('volumechange', this.trackerVolumeChange.bind(this));
    this.adsLoader.player.off('pause', this.pause.bind(this));
    // this.adsLoader.player.off('adplay', this.trackerPlay.bind(this));
    this.adsLoader.player.off('fullscreenchange', this.trackerFullscreenCheck.bind(this));
    window.removeEventListener('beforeunload', this.fireClose.bind(this));
    this.adUi.onAdsPaused();
    this.emit(AdsEvent.Type.COMPLETE);
  }

  getVideoDuration() {
    const duration = this.adsLoader.player.duration();

    return duration;
  }

  trackerInterval(forceComplete) {
    const ad = this.currentCreative;

    if (!ad) {
      return;
    }
    const remainingTime = this.getRemainingTime();
    const duration = this.getVideoDuration() || ad.getDuration();
    const currentTime = this.getCurrentTime();

    let totalAds = 0;
    let adPosition;

    if (ad.getAdPodInfo()) {
      adPosition = ad.getAdPodInfo().getAdPosition();
      totalAds = ad.getAdPodInfo().getTotalAds();
    }
    if (forceComplete) {
      this.adUi.updateAdUi(duration, 0, duration, adPosition, totalAds);
    } else {
      this.adUi.updateAdUi(currentTime, remainingTime, duration, adPosition, totalAds);
    }
  }

  addEventListener(event, callback) {
    this.addListener(event, callback);
  }

  destroy() {
    if (this.vastTracker) {
      this.vastTracker.close();
    }
  }
}
