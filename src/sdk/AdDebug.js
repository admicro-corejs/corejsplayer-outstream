import { EventEmitter } from 'events';
import eventList from './staticEvent/index';

class Debug extends EventEmitter {
  constructor() {
    super();
    this.isDebug = true;
    this.events = [];
    this.listenerForAll = null;
  }

  pushEvent(event, time, value) {
    if (this.events.indexOf(event) < 0) {
      this.events.push(event);
      this.createEvent(event, time, value);
    }
    const clock = new Date().toLocaleTimeString();

    this.emit(event, { event, time, value, clock });
  }

  createEvent(event, time, value) {
    if (typeof this.listenerForAll === 'function') {
      this.on(event, this.listenerForAll);
    }
  }

  autoPlay(message) {
    this.pushEvent('auto-play', 0, message);
  }

  adStarted(time) {
    this.pushEvent('started', time);
  }

  adRequest() {
    this.pushEvent(eventList.AdsEvent.Type.AD_REQUEST);
  }

  adLoaded() {
    this.pushEvent(eventList.AdsEvent.Type.LOADED);
  }

  adError(value) {
    this.pushEvent(eventList.AdsErrorEvent.Type.AD_ERROR, null, JSON.stringify(value));
  }

  adStart(value) {
    this.pushEvent(eventList.AdsEvent.Type.STARTED, null, value);
  }

  adEnd() {
    this.pushEvent('adended');
  }

  adPause(time) {
    this.pushEvent(eventList.AdsEvent.Type.PAUSED, time);
  }

  adResume(time) {
    this.pushEvent(eventList.AdsEvent.Type.RESUMED, time);
  }

  adClick(time) {
    this.pushEvent(eventList.AdsEvent.Type.CLICK, time);
  }

  adMute(time) {
    this.pushEvent(eventList.AdsEvent.Type.VOLUME_MUTED, time);
  }

  adUnmute(time) {
    this.pushEvent(eventList.AdsEvent.Type.VOLUME_CHANGED, time);
  }

  skipAd(time) {
    this.pushEvent(eventList.AdsEvent.Type.SKIPPED, time);
  }

  volummChange(value, time) {
    this.pushEvent(eventList.AdsEvent.Type.VOLUME_CHANGED, time, value);
  }

  trueView(time) {
    this.pushEvent(eventList.AdsEvent.Type.TRUE_VIEW, time);
  }

  /**
   *
   * @param {Object} e : event,value,time
   */
  vastEvent(e) {
    this.pushEvent(e.event, e.time, e.value);
  }

  activeDebug(value) {
    this.isDebug = value;
  }

  listenAllEvent(listener) {
    this.listenerForAll = listener;
  }
}

export default new Debug();
