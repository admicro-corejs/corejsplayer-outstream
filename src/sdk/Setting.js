export default class Setting {
  constructor() {
    this.locale = null;
    this.numRedirects = null;
    this.playerType = null;
    this.playerVersion = null;
    this.autoPlayAdBreaks = null;
  }

  setLocale(locale) {
    this.locale = locale;
  }

  setNumRedirects(numRedirects) {
    this.numRedirects = numRedirects;
  }

  setPlayerType(playerType) {
    this.playerType = playerType;
  }

  setPlayerVersion(playerVersion) {
    this.playerVersion = playerVersion;
  }

  setAutoPlayAdBreaks(autoPlayAdBreaks) {
    this.autoPlayAdBreaks = autoPlayAdBreaks;
  }
}
