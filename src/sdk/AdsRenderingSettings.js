export default class AdsRenderingSettings {
  constructor() {
    this.loadVideoTimeout = 8000;
    this.autoAlign = true;
    this.bitrate = -1;
    this.uiElements = null;
    this.disableClickThrough = false;
    this.enablePreloading = false;
    this.mimeTypes = null;
    this.restoreCustomPlaybackStateOnAdBreakComplete = false;
    this.useLearnMoreButton = false;
    this.useStyledLinearAds = false;
    this.useStyledNonLinearAds = false;
    this.playAdsAfterTime = -1;
    this.useVideoAdUi = true;
    this.disableUi = false;
  }
}
