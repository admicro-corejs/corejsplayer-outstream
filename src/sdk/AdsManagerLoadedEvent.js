export default class AdsManagerLoadedEvent {
  constructor(adsManager, userRequestContext) {
    this.adsManager = adsManager;
    this.userRequestContext = userRequestContext;
  }

  getAdsManager() {
    return this.adsManager;
  }
}
