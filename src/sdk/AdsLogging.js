import window from 'global/window';
import Storage from '../utils/Storage';

export default class Logging {
  constructor(player, viewTracking) {
    this.storage = new Storage();
    this.domainLog = 'https://lg1.logging.admicro.vn/video_track';
    this.key = -1;
    this.dmn = '';
    this.path = '%2F';
    this.ref = '';
    this.avdu = '';
    this.bid = '';
    this.cam = '';
    this.actime = 0;
    this.aview = -1;
    this.advolume = -1;
    this.atype = 5;
    this.vtarget = 0;
    this.pid = -1;
    this.pgid = -1;
    this.guid = -1;
    this.evt = -1;
    this.player = player;
    this.viewTracking = viewTracking;
    this.aidx = 1;
    // this.cqua = 5; //video_change_quality
    // this.bft = 30;
    this.setDomain();
    this.setPath();
    this.setReferrer();
    this.setAtype();
    this.setPid();
    this.setGuid();
    this.setPgid();
  }
  /**
   * video duration
   */
  setKey() {
    this.key = -1;
    return this.key;
  }

  setPgid() {
    this.pgid = window.document.getElementsByTagName('body')[0].dataset.pageid || -1;
  }

  setPid() {
    const pid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = Math.random() * 16 | 0;
      const v = c === 'x' ? r : (r & 0x3 | 0x8);

      return v.toString(16);
    });

    this.pid = pid;
  }

  setGuid() {
    try {
      const uif = this.storage.getCookie('__uif');

      if (!uif) {
        this.guid = -1;
        return;
      }

      this.guid = uif.split('|').filter(i => i.indexOf('__uid') > -1)[0].split(':')[1];
    } catch (error) {
      console.log(error); // eslint-disable-line
      this.guid = -1;
    }
  }

  setDomain() {
    this.dmn = window.encodeURIComponent(`${window.location.hostname}`);
    return this.dmn;
  }

  setPath() {
    this.path = window.encodeURIComponent(`${window.location.pathname}`);
    return this.path;
  }

  setReferrer() {
    this.ref = window.encodeURIComponent(`${window.location.protocol}//${window.document.referrer || window.location.hostname}`);
    return this.ref;
  }

  /**
   * ads duration
   */
  setAvdu() {
    try {
      const duration = new window.Date(this.player.duration() * 1000).toISOString().substr(11, 8);

      this.avdu = encodeURIComponent(duration);
    } catch (error) {
      this.avdu = -1;
    }
  }

  /**
   * banner id
   */
  setBid(id) {
    this.bid = id;
  }

  setAtype() {
    this.atype = 5;
  }

  /**
   * current time of ads
   *
   * @param {} currentTime
   */
  setActime() {
    this.actime = this.player.currentTime();
  }

  /**
   * ratio inside viewport
   */
  setAview() {
    const vis = (function() {
      let stateKey; let eventKey; const keys = {
        hidden: 'visibilitychange',
        webkitHidden: 'webkitvisibilitychange',
        mozHidden: 'mozvisibilitychange',
        msHidden: 'msvisibilitychange'
      };

      for (stateKey in keys) {
        if (stateKey in window.document) {
          eventKey = keys[stateKey];
          break;
        }
      }
      return function(c) {
        if (c) {
          window.document.addEventListener(eventKey, c);
        }
        return !window.document[stateKey];
      };
    })();

    this.aview = vis() ? 1 : -1;
  }

  /**
   * ads volume
   */
  setAdVolume() {
    this.advolume = this.player.muted() ? -1 : this.player.volume();
  }

  setEvt(evt) {
    this.evt = evt;
  }

  createUrlLog() {
    const url = `${this.domainLog}?key=${this.key}&dmn=${this.dmn}&path=${this.path}&pgid=${this.pgid}&ref=${this.ref}&avdu=${this.avdu}&bid=${this.bid}&cam=${this.cam}&atype=${this.atype}&actime=${this.actime}&aview=${this.aview}&advolume=${this.advolume}&vtarget=${this.vtarget}&pid=${this.pid}&guid=${this.guid}&aidx=${this.aidx}&evt=${this.evt}`;

    return url;
  }

  setCam(impressionUrl) {
    if (!impressionUrl) {
      this.cam = -1;
    } else {
      const url = new URL(impressionUrl);
      const cam = url.searchParams.get('p') || -1;

      this.cam = cam;
    }
  }

  logAdStart(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setAdVolume();
    this.setCam(impressionUrl);
    this.setEvt(22);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }

  logAdPause(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setCam(impressionUrl);
    this.setAdVolume();
    this.setEvt(26);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }

  logAdPoll(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setCam(impressionUrl);
    this.setAdVolume();
    this.setEvt(25);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }

  logAd25(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setCam(impressionUrl);
    this.setAdVolume();
    this.setEvt(8);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }

  logAd50(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setCam(impressionUrl);
    this.setAdVolume();
    this.setEvt(9);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }

  logAd75(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setCam(impressionUrl);
    this.setAdVolume();
    this.setEvt(10);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }

  logAd100(bid, impressionUrl) {
    this.bid = bid;
    this.setAvdu();
    this.setActime();
    this.setAview();
    this.setCam(impressionUrl);
    this.setAdVolume();
    this.setEvt(11);
    const img = new window.Image();

    img.src = this.createUrlLog();
  }
}
