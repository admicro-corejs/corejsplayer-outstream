export default class AdsError {
  constructor(error, description) {
    this.error = error;
    this.description = description;
  }
}
