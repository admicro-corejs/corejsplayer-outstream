import AdPodInfo from './AdPodInfo';

export default class Creative {
  constructor(creative, mineType, adPodInfo) {
    this.id = creative.id;
    this.adId = creative.adId;
    this.mineType = mineType;
    this.adPodInfo = adPodInfo;
    this.sequence = creative.sequence;
    this.apiFramework = creative.apiFramework;
    this.trackingEvents = creative.trackingEvents;
    this.type = creative.type;
    this.duration = creative.duration;
    this.skipDelay = creative.skipDelay;
    this.mediaFiles = creative.mediaFiles;
    this.videoClickThroughURLTemplate = creative.videoClickThroughURLTemplate;
    this.videoClickTrackingURLTemplates = creative.videoClickTrackingURLTemplates;
    this.videoCustomClickURLTemplates = creative.videoCustomClickURLTemplates;
    this.adParameters = creative.adParameters;
    this.icons = creative.icon;
  }

  getAdPodInfo() {
    const self = this;
    // default pod for preroll, just for testing.
    const data = {
      podIndex: 0,
      timeOffset: 0,
      totalAds: 1,
      adPosition: 1,
      maxDuration: self.duration
    };

    const adPodInfor = new AdPodInfo(data);

    return adPodInfor;
  }

  isLinear() {
    return this.type === 'linear';
  }

  getDuration() {
    return this.duration;
  }

  getContentType() {
    return this.mineType;
  }
}
