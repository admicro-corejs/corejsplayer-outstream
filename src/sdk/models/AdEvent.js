export default class AdEvent {
  constructor(ad, type) {
    this.ad = ad;
    this.type = type;
  }

  getAd() {
    return this.ad;
  }
}
