export default class AdPodInfo {
  constructor(adPodInfo) {
    const data = adPodInfo || {};

    this.podIndex = data.podIndex;
    this.timeOffset = data.timeOffset;
    this.totalAds = data.totalAds;
    this.adPosition = data.adPosition;
    this.maxDuration = data.maxDuration;
  }

  getAdPosition() {
    return this.adPosition;
  }

  getTotalAds() {
    return this.totalAds;
  }
}
