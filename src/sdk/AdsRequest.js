export default class AdsRequest {
  constructor() {
    this.videoPlayActivation = 'unknown';
    this.videoPlayMuted = 'unknown';
    this.videoContinuousPlay = '0';
    this.liveStreamPrefetchSeconds = 0;
    this.linearAdSlotWidth = 0;
    this.linearAdSlotHeight = 0;
    this.nonLinearAdSlotWidth = 0;
    this.nonLinearAdSlotHeight = 0;
    this.forceNonLinearFullSlot = false;
    this.vastLoadTimeout = 5000;
    this.adTagUrl = '';
    this.vastXml = '';
    this.viewable = null;
  }

  /**
   *
   * @param {string} value
   */
  setAdWillAutoPlay(value) {
    this.videoPlayActivation = value;
  }

  /**
   *
   * @param {string} value
   */
  setAdWillPlayMuted(value) {
    this.videoPlayMuted = value;
  }
}
